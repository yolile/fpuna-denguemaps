package com.app.utils.tables.dao;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import au.com.bytecode.opencsv.CSVWriter;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.app.domain.Notificacion;
import com.app.utils.tables.QueryBuilder;
import com.app.utils.tables.annotation.AttributeDescriptor;
import com.app.utils.tables.filter.BaseFilter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by rparra on 23/3/15.
 */
public class GenericDaoImpl<T> implements GenericDao<T> {
    protected static Logger log = Logger.getLogger(GenericDaoImpl.class.getName());

	@Inject
	private EntityManager em;
    
    @Override
    public List<T> getEntities(List<String> attributes, List<List<BaseFilter<?>>> filters, Integer pageSize, Integer offset) {
        List<String> columns = getColumns(attributes);
        QueryBuilder<T> builder = new QueryBuilder<>(getEntityClass());
        String query = getQuery(builder, columns, filters, pageSize, offset);
        System.out.println(query);
        
        Class _class;
        List<T> entities = null;
		try {
			_class = Class.forName(this.getEntityClass().getCanonicalName());
			System.out.println("+++++++++++++++++++++ _class " + this.getEntityClass().getCanonicalName());
			//System.out.println("+++++++++++++++++++++ _class " + _class);
			entities = em.createNativeQuery(query, _class).getResultList();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se encuentra la clase " + this.getEntityClass().getCanonicalName());
		}
		
		return entities;
		
    }
    
	private List<T> getEntitiesFromResultSet(ResultSet resultSet)
			throws SQLException {
		List<Object> entities = new ArrayList<Object>();
		String columnName = "";
		try {
			Class _class = Class.forName(this.getEntityClass().getCanonicalName());
			//System.out.println(" ++++++++++++++++++++ _class " + _class);
			Map<String, Field> columns = new HashMap<String, Field>();
			for (Field f: _class.getDeclaredFields()) {
				AttributeDescriptor descriptor = f.getAnnotation(AttributeDescriptor.class);
				if (descriptor != null) {
					columns.put(descriptor.path(), f);
				}
			}
			
			while (resultSet.next()) {
				ResultSetMetaData metaData = resultSet.getMetaData();
				int columnCount = metaData.getColumnCount();
				Object entity = _class.newInstance();
				// System.out.println(" ++++++++++++++++++++ entity " + entity);
				for (int i = 1; i <= columnCount; ++i) {
					columnName = metaData.getColumnName(i).toLowerCase();
					Object object = resultSet.getObject(i);
					// System.out.println("+++++++++++++++++ columnName " +
					// columnName);
					Field f = columns.get(columnName);
					f.setAccessible(true);
					f.set(entity, object);
				}
				entities.add(entity);
			}
		} catch (ClassNotFoundException e) {
			String msg = "No se existe la clase " + this.getEntityClass().getCanonicalName();
            //log.error(msg);
            log.info(msg);
            throw new RuntimeException(msg);
		} catch (InstantiationException e) {
			String msg = "No se pudo instanciar la clase " + this.getEntityClass().getCanonicalName();
            //log.error(msg);
            log.info(msg);
            throw new RuntimeException(msg);
		} catch (IllegalAccessException e) {
			String msg = "No se puede acceder al atributo " + columnName + " de la clase " + this.getEntityClass().getCanonicalName();
            //log.error(msg);
            log.info(msg);
            throw new RuntimeException(msg);
		} catch (SecurityException e) {
            //log.error(e);
            log.info(e.toString());
		}
		
		return (List<T>) entities;
	}

    @Override
    public Integer getEntitiesCount() {
    	QueryBuilder<T> builder = new QueryBuilder<>(getEntityClass());
    	String countQuery = builder.buildTotalCount();
    	System.out.println(countQuery);
    	BigInteger count = (BigInteger) em.createNativeQuery(countQuery).getSingleResult();
    	System.out.println("count: "  + count);
    	return count.intValue();
    	
    }
    
    @Override
    public Integer getFilteredEntitiesCount(List<String> attributes, List<List<BaseFilter<?>>> filters) {
        List<String> columns = getColumns(attributes);
        QueryBuilder<T> builder = new QueryBuilder<>(getEntityClass());
        builder.addAllFilters(filters);
        String countQuery = builder.buildCount();
        System.out.println(countQuery);
        BigInteger count = (BigInteger)  em.createNativeQuery(countQuery).getSingleResult();
        System.out.println("filteredCount: "  + count);
		return count.intValue();
    }

    @Override
    public String getAllFilteredEntities(List<List<BaseFilter<?>>> filters) {
        List<String> columns = new ArrayList<>();
        QueryBuilder<T> builder = new QueryBuilder<>(getEntityClass());
        builder.addAllFilters(filters);
        String query = builder.buildSelectAll();
        
        Class _class;
        List<T> entities = null;
		try {
			_class = Class.forName(this.getEntityClass().getCanonicalName());
			System.out.println("+++++++++++++++++++++ _class " + _class);
			entities = em.createNativeQuery(query, _class).getResultList();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se encuentra la clase " + this.getEntityClass().getCanonicalName());
		}
		
		if (entities != null) {
			//Gson gson = new Gson();
			Gson gson = new GsonBuilder().serializeNulls().create();
			return gson.toJson(entities);
		}
		
		return null;

    }


    private String getQuery(QueryBuilder<T> builder, List<String> columns, List<List<BaseFilter<?>>> filters, Integer pageSize, Integer offset){
        builder.addAllFilters(filters);
        builder.addColumns(columns);
        if(pageSize != null){
            builder.setPageSize(pageSize);
        }
        if(offset != null){
            builder.setOffset(offset);
        }
        return builder.build();
    }

    protected List<String> getColumns(List<String> attributes){
        List<String> result = new ArrayList<>();
        for(String attr: attributes){
            result.add(getColumn(attr));
        }
        return  result;
    }

    private String getColumn(String attrName){
        try {
            Field f =  this.getEntityClass().getDeclaredField(attrName);
            AttributeDescriptor descriptor = f.getAnnotation(AttributeDescriptor.class);
            if (descriptor.path() == null) {
                /*Convencion sobre configuracion, se asume que la columna de la BD se llama como el atributo, pero
                pasando de camelCase a camel_case.*/
                return attrName.replaceAll("(.)(\\p{Upper})", "$1_$2").toLowerCase();
            } else {
                return descriptor.path();
            }
        } catch (NoSuchFieldException e) {
            String msg = "No existe el atributo " + attrName + " en la clase " + this.getEntityClass().getCanonicalName();
            //log.error(msg);
            log.info(msg);
            throw new RuntimeException(msg);
        }
    }

    @SuppressWarnings("unchecked")
    private Class<T> getEntityClass() {

        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        return (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

}
