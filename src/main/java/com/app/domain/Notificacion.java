package com.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.app.utils.tables.annotation.AttributeDescriptor;

@Entity
public class Notificacion implements Serializable {

	private static final long serialVersionUID = 1L;

	@AttributeDescriptor(path = "id")
	@Id
	@Column
	private int id;

	@AttributeDescriptor(path = "semana")
	@Column
	private String semana;

	@AttributeDescriptor(path = "anio")
	@Column
	private String anio;

	@AttributeDescriptor(path = "fecha_notificacion")
	@Column
	private String fecha_notificacion;

	@AttributeDescriptor(path = "edad")
	@Column
	private String edad;

	@AttributeDescriptor(path = "sexo")
	@Column
	private String sexo;

	@AttributeDescriptor(path = "departamento")
	@Column
	private String departamento;

	@AttributeDescriptor(path = "distrito")
	@Column
	private String distrito;

	@AttributeDescriptor(path = "clasificacon_clinica")
	@Column
	private String clasificacon_clinica;

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getFecha_notificacion() {
		return fecha_notificacion;
	}

	public void setFecha_notificacion(String fecha_notificacion) {
		this.fecha_notificacion = fecha_notificacion;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getClasificacon_clinica() {
		return clasificacon_clinica;
	}

	public void setClasificacon_clinica(String clasificacon_clinica) {
		this.clasificacon_clinica = clasificacon_clinica;
	}

}
