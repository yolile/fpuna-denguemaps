#!/bin/bash
INSTANCE=$1
grunt build --force
mkdir dist/images
cp app/images/* dist/images/
mkdir dist/fonts
cp app/theme/assets/fonts/* dist/fonts/
mkdir dist/styles/images
cp bower_components/mapbox.js/images/* dist/styles/images/
chmod -R 755 dist
if [ $INSTANCE ]
then
	echo "Copiando a $INSTANCE... "
	cp -R dist/* $INSTANCE
fi
