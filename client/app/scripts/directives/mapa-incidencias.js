'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaIncidencias', function ($rootScope,  $http, mapServices, $q) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			detalle:'='
		},
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		//mapCtrl.imprimir();
			mapCtrl.imprimir("mapaIncidencias");
			mapCtrl.cleanMap();

			var departamentos;
			var map = mapCtrl.getMap();
			var STATE = $rootScope.INCIDENCIA_STATE;
			console.log("STATE::");
			console.log($rootScope.INCIDENCIA_STATE);
			if(STATE === undefined){
				console.log("undefined::");
				STATE = {};
				STATE.inZoom = false;
				departamentos = mapCtrl.getDepartamentos();
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature: onEachFeature}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					return init($("#sMonth").val(), $("#sYear").val(), STATE.geoJsonLayer);
				}).then(function(data){
      		getIncidenciasLegend().addTo(map);
		      getIncidenciasInfo().addTo(map);
      		data.setStyle(getStyle);
      		$rootScope.INCIDENCIA_STATE = STATE;
          STATE.info.update();
      	});
	        mapServices.getPoblacionPorDepto($("#sYear").val())
		    		.then(function(data){
		    			STATE.poblacion = data;
		    		}).catch(function(err){
	    				console.log(err);
	    		});
	    		mapServices.getPoblacionPorDistrito($("#sYear").val())
		    		.then(function(data){
		    			STATE.poblacion_distrito = data;
		    		}).catch(function(err){
	    				console.log(err);
	    		});
		    	mapServices.getPoblacionPorBarrioAsu()
		    		.then(function(data){
		    			STATE.poblacion_asu = data;
		    		}).catch(function(err){
	    				console.log(err);
	    		});

			} else {
				STATE.geoJsonLayer.addTo(map);
				if(STATE.inZoom){
					STATE.drillDownLayer.addTo(map);
					map.fitBounds(STATE.drillDownLayer.getBounds());
					STATE.backButton.addTo(map);
					mapCtrl.addControl(STATE.backButton);
					mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
				}
				STATE.info.addTo(map);
				STATE.incidenciaLegend.addTo(map);
				mapCtrl.addControl(STATE.info);
				mapCtrl.addControl(STATE.incidenciaLegend);
				mapCtrl.setActualLayer(STATE.geoJsonLayer);
        STATE.info.update();
			}

			/* Control de opacidad */
			//Create the opacity controls
	        /*var opacitySlider = new L.Control.opacitySlider();
	        map.addControl(opacitySlider);

	    	//Specify the layer for which you want to modify the opacity. Note that the setOpacityLayer() method applies to all the controls.
	    	//You only need to call it once.
	        opacitySlider.setOpacityLayer(departamentos);

	    	//Set initial opacity to 0.5 (Optional)
        	departamentos.setOpacity(0.5);*/
        	//mapUtil.setMap(map);

        	$("#sMonth").on({
				slide: function(){
					reloadSem($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});


			var backClass =  L.Control.extend({
		        options: {
		            position: 'topright'
		        },

		        onAdd: function (map) {
		            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
		            L.DomEvent
		                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
		                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
		            .addListener(controlDiv, 'click', drillUp);

		            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-interior btn btn-sm btn-primary', controlDiv);
		            controlUI.title = 'Volver a incidencias por dptos';
		            return controlDiv;
		        }
		    });


			function init(sem, ano, layer){
	  			var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			return mapServices.getIncidenciaDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosIncidencia =  data;
	    				return mapServices.getIncidenciaDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					STATE.distritosIncidencia = data;
	  					return mapServices.getIncidenciaAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					STATE.asuncionIncidencia = data;
	  					reloadSem(STATE.semana);
	  					defered.resolve(layer);
	  					return promise;
	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});

	  		}

	  		function reloadAnio(sem, ano){
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			mapServices.getIncidenciaDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosIncidencia =  data;
	    				return mapServices.getIncidenciaDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					STATE.distritosIncidencia = data;
	  					return mapServices.getIncidenciaAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					STATE.asuncionIncidencia = data;
	  					reloadSem(STATE.semana);

	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	    		mapServices.getPoblacionPorDepto(STATE.anio)
		    		.then(function(data){
		    			STATE.poblacion = data;
		    		}).catch(function(err){
	    				console.log(err);
	    			});
	    		mapServices.getPoblacionPorDistrito(STATE.anio)
		    		.then(function(data){
		    			STATE.poblacion_distrito = data;
		    		}).catch(function(err){
	    				console.log(err);
	    			});
	  		}



			function reloadSem(semana){
				STATE.semana = semana;
			    var rasu;
			    var rcen;
			    var mapSem = new Object();
			    var mapSemDis = new Object();

			    //Cargar el json de los incidencias por semana
			    //incidencias de todas las semanas de los departamentos
			    var incidencia = STATE.departamentosIncidencia;
			    //incidencias de todas las semana de los distritos
			    var incidenciaDis = STATE.distritosIncidencia;
			    //incidencias de todas las semanas de los barrios de asuncion
			    var incidenciaAsu = STATE.asuncionIncidencia;

			    for(var i=0; i<incidencia.length; i++){
			        var obj = incidencia[i];
			        if(obj["semana"]== semana ){
			            mapSem[obj["departamento"]]= obj;
			            if(obj["departamento"]=="CENTRAL"){
			                rcen = obj;
			            }
			            if(obj["departamento"]=="ASUNCION"){
			                rasu = obj;
			            }
			        }
			    }
			    //console.log(rasu);

			    //incidencia de los departamentos de la semana seleccionada
			    STATE.incidenciaSemanaDep = mapSem;
			   //console.log(mapSem);
			    for(var i=0; i<incidenciaDis.length; i++){
			        var obj = incidenciaDis[i];
			        if(obj["semana"]== semana ){
			            mapSemDis[obj["distrito"]]= obj;
			        }
			    }
			    //Incluir en el mapSemDis los barrios de Asuncion

			    for(var i=0; i<incidenciaAsu.length; i++){
			        var obj = incidenciaAsu[i];
			        if(obj["semana"] == semana){
			            mapSemDis[obj["barrio"]] = obj;
			        }

			    }
			    //console.log(mapSemDis);
			   	//incidencias de los distritos de la semana seleccionada
			    STATE.incidenciaSemanaDis = mapSemDis;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    if(STATE.inZoom){
			    	STATE.drillDownLayer.setStyle(getStyleDrillDown);
			    }
			}

	  		function getIncidenciasLegend(){
	  			STATE.incidenciaLegend = L.control({
			        position: 'bottomright'
			    });
			    STATE.incidenciaLegend.onAdd = function(map) {
			    	this._div = L.DomUtil.create('div', 'info legend');
			        this.update();
			        return this._div;
			    };
			    STATE.incidenciaLegend.update = function(){

			    	if(STATE.inZoom){
			    		this._div.innerHTML = '<span>Tasa de incidencia</span><br/><small>cada 10.000 hab.</small>'
			    			+'<br><i style="background:' + getColor(30) + '"></i> ' + '>= 30'
				        	+'<br><i style="background:' + getColor(21) + '"></i> ' + '20 - 30' // >200
				        	+'<br><i style="background:' + getColor(11) + '"></i> ' + '10 - 20' // >100
				        	+'<br><i style="background:' + getColor(9)  + '"></i> ' + '<10' // <100
				        	+'<br><i style="background:' + getColor(0) + '"></i> ' + '0'; // == 0
			    	} else {
			    		this._div.innerHTML = '<span>Tasa de incidencia</span><br/><small>cada 100.000 hab.</small>'
			    			+'<br><i style="background:' + getColor(300) + '"></i> ' + '>= 300'
				        	+'<br><i style="background:' + getColor(210) + '"></i> ' + '200 - 300' // >200
				        	+'<br><i style="background:' + getColor(110) + '"></i> ' + '100 - 200' // >100
				        	+'<br><i style="background:' + getColor(10) + '"></i> ' + '< 100' // <100
				        	+'<br><i style="background:' + getColor(0) + '"></i> ' + '0'; // == 0
			    	}

			    };
			    mapCtrl.addControl(STATE.incidenciaLegend);
			    return STATE.incidenciaLegend;
	  		}

	  		function getIncidenciasInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info info-layer');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {

			        if(props){
			            var dep = props.dpto_desc;
			            var mapSem = STATE.incidenciaSemanaDep;
			            var nroNS = '0';
			            var pob = '-';
			            var casos = '-';

		                pob = formattNumber(STATE.poblacion[parseInt(props.dpto)].total);

			            try{
			                nroNS = mapSem[dep]["incidencia"].toString().replace(/\./g, ',');
			                casos = mapSem[dep]["casos"];
			            }catch(e){

			            }
			          this._div.innerHTML =  '<b>Mapa de Incidencia Dengue</b><br/>'
			          					+ '<b>Año:</b> '+ STATE.anio
			          					+ ' - <b>Semana:</b> ' + STATE.semana
			          					+ '<br/><b>Dpto:</b> ' + dep
			          					+ '<br/><b>Incidencia:</b> ' + nroNS + '<small>/100.000 hab.</small><br/>'
			          					+ '<b>Población:</b> ' + pob + ' '
			          					+ '<b>Casos:</b> ' + casos + '<br/>'
			          					+ '<small>(sospechosos+confirmados)</small><br/>';
			        } else {
                this._div.innerHTML = '<b>Mapa de Incidencia Dengue</b><br/>';
              }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){
			            var dep = props.dpto_desc, depAsu = props.dpto_desc, dis = props.dist_desc;
			            var mapSem = STATE.incidenciaSemanaDis, nroNS = '0', pob = '-', casos = '-', info;
			            if(depAsu == 'ASUNCION'){
			            	dis = props.dist_desc;
			                var key = depAsu+'-'+props.barlo_desc;
			                var keyP = parseInt(props.bar_loc).toString();
			                pob = formattNumber(STATE.poblacion_asu[keyP.toString()].total);
			                try{
			                    nroNS = mapSem[key]["incidencia"].toString().replace(/\./g, ',');
			                    casos = mapSem[key]["casos"];
			                    pob = mapSem[key]["poblacion"];
			                }catch(e){

			                }
			                info =  '<b>Mapa de Incidencia Dengue</b><br/>'
			                	+ '<b>Año: </b>' + STATE.anio
			                	+ ' - <b>Semana: </b>' + STATE.semana
			                	+ '<br/><b>Dpto: </b>' + depAsu
			                	+ '<br/><b>Distrito: </b>' + dis
			                	+ '<br/><b>Barrio:</b> ' + props.barlo_desc
			                	+ '<br/><b>Incidencia:</b> '+ nroNS +'<small>/10.000 hab.</small>'
			                	+ '<br/><b>Población:</b> ' + pob
			          			+ ' <b>Casos:</b> ' + casos + '<br/>'
			          			+ '<small>(sospechosos+confirmados)</small><br/>';
			            } else {
			                var key = dep+'-'+dis;
			                var keyP = parseInt(props.dpto).toString() + parseInt(props.distrito).toString();
			                pob = formattNumber(STATE.poblacion_distrito[keyP.toString()].total);
			                try{
			                    nroNS = mapSem[key]["incidencia"].toString().replace('\.', ',');
			                    casos = mapSem[key]["casos"];

			                }catch(e){

			                }
			                info = '<b>Mapa de Incidencia Dengue</b><br/>'
			                	+ '<b>Año:</b> ' + STATE.anio
			                	+' - <b>Semana:</b> '+ STATE.semana
			                	+ '<br/><b>Dpto:</b> ' + dep
			                	+ '<br/><b>Distrito:</b> ' + dis
			                	+ '<br/><b>Incidencia:</b> ' + nroNS + '<small>/10.000 hab.</small>'
			                	+ '<br><b>Población:</b> ' + pob
			          			+ ' <b>Casos:</b> ' + casos + '<br/>'
			          			+ '<small>(sospechosos+confirmados)</small><br/>';
			            }
			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }

			function onEachFeature(feature, layer) {
			    layer.on({
			        mouseover: mouseover,
			        mouseout: mouseout,
			        click: zoomToFeature
			    });

			}

			function mouseover(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.update(layer.feature.properties);
			}

			/*Evento al salir el puntero de un departamento*/
			function mouseout(e) {
			    //geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();

			}

			/*Zoom al hacer click en un departamento*/
			function zoomToFeature(e) {

			    var target = e.target;

			    if(!STATE.inZoom){
				   	STATE.backButton = new backClass();
			        map.addControl(STATE.backButton);
			        mapCtrl.addControl(STATE.backButton);
			    }
			    if(target.feature.properties.dpto_desc == 'ASUNCION'){
			    	 mapServices.getBarrios(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());

					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					   	STATE.incidenciaLegend.update();
	    			});
			    } else {
			    	mapServices.getDistritos(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					    STATE.incidenciaLegend.update();
	    			});
			    }



			}

	    	function getColor(d) {
	    		if(STATE.inZoom){
	    			return d >= 30 ? '#FE0516' :
			        d >= 20 ? '#FF6905' :
			        d >= 10 ? '#FFB905' :
			        d == 0 ? '#FEFEFF':
			        '#FFF96D';
	    		}

			    return d >= 300 ? '#FE0516' :
			        d >= 200 ? '#FF6905' :
			        d >= 100 ? '#FFB905' :
			        d == 0 ? '#FEFEFF':
			        '#FFF96D';
			}

			/*Estilo de la capa de departamento de acuerdo a los valores de incidencia*/
			function getStyle(feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.incidenciaSemanaDep;
			    var color = 0;
			    try{
			        color = mapSem[n]["incidencia"];
			    }catch(e){
			    }
			    if(STATE.inZoom){
			    	return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0,
			            fillColor: getColor(color)
			        };
			    }
			  	else{
			  		return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			           	fillOpacity: 0.5,
			            fillColor: getColor(color)
			        };
			  	}

			}

			/*Estilo de la capa de distritos de acuedo a los valores de incidencia*/
			function getStyleDrillDown(feature) {
			    var prop = feature.properties;
			    var n = prop.dpto_desc+'-'+prop.dist_desc;
			    var mapSem = STATE.incidenciaSemanaDis;
			    var color = 0;
			    if(prop.dpto_desc == 'ASUNCION'){
			        n = prop.dpto_desc+'-'+prop.barlo_desc;
			    }
			   try{
			        color = mapSem[n]["incidencia"];

			    }catch(e){
			    }

			    return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0.5,
			            fillColor: getColor(color)
			        };
			}
			/*Eventos para cada distrito*/
			function onEachFeatureDrillDown(feature, layer) {
			    layer.on({
			        mouseover: mouseoverDrillDown,
			        mouseout: mouseoutDrillDown,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverDrillDown(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.updateDrillDown(layer.feature.properties);
			}
			/*Evento al salir el puntero de un distrito*/
			function mouseoutDrillDown(e) {
			    STATE.drillDownLayer.resetStyle(e.target);
			    STATE.info.updateDrillDown();

			}
			function drillUp(){
			    //map.fitBounds(SMV.STATE.geoJsonLayer.getBounds());
			    map.removeLayer(STATE.drillDownLayer);
			    STATE.drillDownLayer = undefined;
			   	STATE.inZoom = false;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    STATE.backButton.removeFrom(map);
			    mapCtrl.removeControl(STATE.backButton);
			    map.setView([-23.388, -60.189], 7);
			    STATE.incidenciaLegend.update();
			}

			function formattNumber(x) {
			    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
			}

	    }
	};
});
