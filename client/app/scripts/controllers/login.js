'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('AuthCtrl', function ($scope, $rootScope, $location, AuthenticationService) {

	  	$scope.logout = function () {
	  		//console.log("entro a logout");
		    $scope.dataLoading = true;
		    AuthenticationService.ClearCredentials();
		    /* Falta hacer que se quede en la misma pagina pero que refresque */
		    $location.path('/');
	    }

	    $scope.isLoggedIn = function () {
	  		//console.log("entro a isLoggedIn");
	  		if ($rootScope.globals.currentUser) return false;
		   	else return true;
	    }

});