'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('FormularioCtrl', function ($scope, mapServices) {
  	$scope.dptos = [{id:'CAPITAL', name:'Capital'},
  				{id:'CENTRAL', name:'Central'},
  				{id:'ALTO PARANA', name:'Alto Paraná'},
  				{id:'ITAPUA', name:'Itapúa'}];
  	$scope.reg = {
  		dpto:null,
  		dist:null,
  		barrio:null,
  		brote:'SI',
  		cant_0:null,
  		cant_1:null,
  		cant_2:null,
  		incidencia:null,
  		periodo:null,
  		anio:'2016',
  		semana:null
  	};
  	$scope.verResultado = false;
  	var distCA = [{id:'ASUNCION', name:'Asunción'}];
  	var distCE = [{id:"SAN ANTONIO", name:'San Antonio'},
				{id:"LIMPIO", name:'Limpio'},
				{id:"VILLETA", name:'Villeta'},
				{id:"YPANE", name:'Ypane'},
				{id:"YPACARAI", name:'Ypacarai'},
				{id:"ÑEMBY", name:'Ñemby'},
				{id:"FERNANDO DE LA MORA", name:'Fernando de la Mora'},
				{id:"LUQUE", name:'Luque'},
				{id:"AREGUA", name:'Arguá'},
				{id:"J A SALDIVAR", name:'J. A. Saldivar'},
				{id:"ITA", name:'Itá'},
				{id:"ITAUGUA", name:'Itagua'},
				{id:"NUEVA ITALIA", name:'Nueva Italia'},
				{id:"GUARAMBARE", name:'Guarambaré'},
				{id:"CAPIATA", name:'Capiatá'},
				{id:"SAN LORENZO", name:'San Lorenzo'},
				{id:"VILLA ELISA", name:'Villa Elisa'},
				{id:"LAMBARE", name:'Lambaré'},
				{id:"MARIANO ROQUE ALONSO", name:'Mariano Roque Alonso'}];
	var distAP = [{id:"SAN CRISTOBAL", name:'San Cristobal'},
				{id:"SANTA RITA", name:'Santa Rita'},
				{id:"ITAKYRY", name:'Itakyry'},
				{id:"MINGA PORA", name:'Minga Porä'},
				{id:"SAN ALBERTO", name:'San Alberto'},
				{id:"CIUDAD DEL ESTE", name:'Ciudad del Este'},
				{id:"YGUAZU", name:'Yguazu'},
				{id:"SANTA FE DEL PARANA", name:'Santa Fe del Paraná'},
				{id:"PRESIDENTE FRANCO", name:'Presidente Franco'},
				{id:"HERNANDARIAS", name:'Hernandarias'},
				{id:"TAVAPY", name:'Tavapy'},
				{id:"LOS CEDRALES", name:'Los Cedrales'},
				{id:"MBARACAYU", name:'Mbaracayo'},
				{id:"IRUÑA", name:'Iruña'},
				{id:"NARANJAL", name:'Naranjal'},
				{id:"DOMINGO MARTINEZ DE IRALA", name:'Domingo Martinez de Irala'},
				{id:"DR. JUAN LEON MALLORQUIN", name:'Dr. Juan León Mallorquín'},
				{id:"SANTA ROSA DEL MONDAY", name:'Santa Rosa del Monday'},
				{id:"MINGA GUAZU", name:'Minga Guazu'},
				{id:"ÑACUNDAY", name:'Ñacunday'},
				{id:"JUAN E. O'LEARY", name:'Juan E. O\'leary'}];
	var distIT = [{id:"ENCARNACION", name:'Encarnación'}];
	var barrios = [{id:"SAN VICENTE"},
				{id:"REPUBLICANO"},
				{id:"PINOZA"},
				{id:"CIUDAD NUEVA"},
				{id:"TTE. SILVIO PETTIROSSI"},
				{id:"GRAL JOSE EDUVIGIS DIAZ"},
				{id:"SAN ROQUE"},
				{id:"OBRERO INTENDENTE B. GUGGIARI"},
				{id:"GRAL BERNARDINO CABALLERO"},
				{id:"MBURICAO"},
				{id:"NAZARETH"},
				{id:"TERMINAL"},
				{id:"HIPODROMO"},
				{id:"SAN PABLO"},
				{id:"TEMBETARY"},
				{id:"LOS LAURELES"},
				{id:"VILLA AURELIA"},
				{id:"MCAL. JOSE FELIX ESTIGARRIBIA"},
				{id:"SAN CRISTOBAL"},
				{id:"LUIS ALBERTO DE HERRERA"},
				{id:"SANTA MARIA"},
				{id:"YTAY"},
				{id:"ÑU GUAZU"},
				{id:"SALVADOR DEL MUNDO"},
				{id:"SAN JORGE"},
				{id:"YCUA SATI"},
				{id:"VILLA MORRA"},
				{id:"RECOLETA"},
				{id:"MANORA"},
				{id:"SANTO DOMINGO"},
				{id:"LAS LOMAS"},
				{id:"CAÑADA DEL YBYRAY"},
				{id:"MBOCAYATY"},
				{id:"MADAME ELISA ALICIA LINCH"},
				{id:"MBURUCUYA"},
				{id:"LOMA PYTA"},
				{id:"BELLA VISTA"},
				{id:"MARISCAL FRANCISCO SOLANO LOPEZ"},
				{id:"VIRGEN DEL HUERTO"},
				{id:"VIRGEN DE LA ASUNCION"},
				{id:"SANTISIMA TRINIDAD"},
				{id:"PRESIDENTE CARLOS ANTONIO LOPEZ"},
				{id:"ROBERTO L. PETIT"},
				{id:"LA CATEDRAL"},
				{id:"LA ENCARNACION"},
				{id:"RICARDO BRUGADA"},
				{id:"SAN BLAS"},
				{id:"DE LA RESIDENTA"},
				{id:"VISTA ALEGRE"},
				{id:"SAN CAYETANO"},
				{id:"SANTA ANA"},
				{id:"TACUMBU"},
				{id:"SAJONIA"},
				{id:"ITA PYTA PUNTA"},
				{id:"SAN ANTONIO"},
				{id:"DR. GASPAR RODRIGUEZ DE FRANCIA"},
				{id:"BOTANICO"},
				{id:"SANTA ROSA"},
				{id:"VIRGEN DE FATIMA"},
				{id:"TABLADA NUEVA"},
				{id:"JARA"},
				{id:"SANTA LIBRADA"},
				{id:"BAÑADO CARA CARA"},
				{id:"ZEBALLOS CUE"},
				{id:"ITA ENRAMADA"},
				{id:"BANCO SAN MIGUEL"},
				{id:"LAS MERCEDES"},
				{id:"JUKYTY"}];

  	$scope.reg.dpto=$scope.dptos[0];
  	$scope.distritos = distCA;
  	$scope.reg.dist = $scope.distritos[0];
  	$scope.barrios = barrios;
  	//$scope.reg.barrio = barrios[0];
  	$scope.prediccion = null;
  	$scope.reg.anio=2016;
  	$scope.reg.semana=1;
  	var mapUbicacion = new Object();
  	mapUbicacion[$scope.dptos[0].id] = distCA;
  	mapUbicacion[$scope.dptos[1].id] = distCE;
  	mapUbicacion[$scope.dptos[2].id] = distAP;
  	mapUbicacion[$scope.dptos[3].id] = distIT;
  	 $scope.master = {};

      $scope.guardar = function(registro) {
        /*if($scope.reg.dist.id!='ASUNCNION'){
        	$scope.reg.barrio = null;
        }*/
        mapServices.esBrote(registro).then(function(data){
      		$scope.prediccion = data;
      		console.log('prediccion:: ', $scope.prediccion );
      		$scope.verResultado =true;
      	});
      };

      $scope.reset = function() {
        $scope.reg = angular.copy($scope.master);
        $scope.reg.dpto=$scope.dptos[0];
        $scope.reg.dist = $scope.distritos[0];
        $scope.reg.brote = 'SI';
      };

      $scope.reset();

      $scope.updateDist = function(){
      	$scope.distritos = mapUbicacion[$scope.reg.dpto.id];
      	$scope.reg.dist = $scope.distritos[0];
      };

      $scope.volver =  function(){
      	$scope.reset();
      	$scope.verResultado = false;
      };

});
