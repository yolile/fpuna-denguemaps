'use strict';

/**
 * @ngdoc overview
 * @name denguemapsApp
 * @description
 * # denguemapsApp
 *
 * Main module of the application.
 */

angular
  .module('denguemapsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'datatables',
    'datatables.bootstrap',
    'ui.bootstrap',
    'angularSpinner',
    'flow',
    'toastr'
  ])
  .config(["$routeProvider", function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/listado', {
        templateUrl: 'views/listado.html',
        controller: 'ListadoCtrl'
      })
      .when('/diccionario', {
        templateUrl: 'views/diccionario.html',
        controller: 'DiccionarioCtrl'
      })
      /*.when('/acercade', {
        templateUrl: 'views/acercade.html',
        controller: 'AcercaDeCtrl'
      })
      .when('/datos', {
        templateUrl: 'views/datos.html',
        controller: 'DatosCtrl',
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'AuthCtrl'
      })*/
      .otherwise({
        redirectTo: '/'
      });
  }])
 .run(["$rootScope", "$location", "$cookieStore", "$http", function ($rootScope, $location, $cookieStore, $http) {
    /* Control de paginas privadas */
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }

    $rootScope.$on('$routeChangeStart', function (ev, next, curr) {
     /* if (next.$$route.originalPath == '/datos') {
        if ($rootScope.globals.currentUser) { $location.path('/datos');}
        else $location.path('/login');
      }*/

    })
  }]);
 //window.basePath = "http://www.cds.com.py";
 window.basePath = "http://127.0.0.1:8084";

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('MainCtrl', ["$scope", "$rootScope", function ($scope, $rootScope) {
       
       /* $scope.isActiveTab = function (tab) {
          console.log("dentro de isActiveTab");
          if ($rootScope.tab == tab) {
            return true;
          } else if (!$rootScope.tab && tab=='map5') {
            return true;
          } else {
            return false;
          }
        }

        //$rootScope.tab.on('change', setActiveTab);

        var MECONF = MECONF || {};
        MECONF.tilesLoaded = false;

        MECONF.LAYERS = function () {
            var mapbox = L.tileLayer(
                    'http://api.tiles.mapbox.com/v4/rparra.jmk7g7ep/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg');
            var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {minZoom: 3});
            var gglHybrid = new L.Google('HYBRID');
            var gglRoadmap = new L.Google('ROADMAP');
            return {
                MAPBOX: mapbox,
                OPEN_STREET_MAPS: osm,
                GOOGLE_HYBRID: gglHybrid,
                GOOGLE_ROADMAP: gglRoadmap
            }
        };

        var finishedLoading = function() {
          if(tilesLoaded){
            $(".spinner").remove();
            MECONF.tilesLoaded = false;
          }

        };

        var startLoading = function() {
          var spinner = new Spinner({
              color: "#ffb885",
              radius: 10,
              width: 5,
              length: 10,
              top: '92%',
              left: '98%'
          }).spin();
          $("#map").append(spinner.el);
        };

        var setup_gmaps = function() {
          google.maps.event.addListenerOnce(this._google, 'tilesloaded', tilesLoaded);
        };

        var tilesLoaded = function(){
          MECONF.tilesLoaded = true;
          finishedLoading();
        }

      startLoading();

      L.mapbox.accessToken = 'pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg';
      var layers = MECONF.LAYERS();
      var mapbox = layers.MAPBOX.on('load', tilesLoaded);
      var osm = layers.OPEN_STREET_MAPS.on('load', tilesLoaded);

      var gglHybrid = layers.GOOGLE_HYBRID.on('MapObjectInitialized', setup_gmaps);
      var gglRoadmap = layers.GOOGLE_ROADMAP.on('MapObjectInitialized', setup_gmaps);

      /* Layer para probar opacidad */
      /*var historic_seattle = new L.tileLayer.wms('http://demo.lizardtech.com/lizardtech/iserv/ows', {
              layers: 'Seattle1890',
              maxZoom: 18,
              format: 'image/png',
              transparent: true
          });*/

     /* var map = L.map('map', {maxZoom: 18, minZoom: 3, worldCopyJump: true, attributionControl: false, zoomControl: false})
            .setView([-24, -57.189], 7)
            //.setView([47.59, -122.30], 12)
            .on('baselayerchange', startLoading);
      new L.Control.Zoom({ position: 'topright' }).addTo(map);

      var baseMaps = {
        'Calles OpenStreetMap': osm,
        'Terreno': mapbox,
        'Satélite': gglHybrid,
        'Calles Google Maps': gglRoadmap
      };

      map.addLayer(gglRoadmap);

      //var sidebar = L.control.sidebar('sidebar').addTo(map);

      MECONF.map = map;
      $rootScope.MAP = MECONF.map;

      console.log(MECONF.map);

      console.log("MAIN CONTROLLER");*/
}]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('AcercaDeCtrl', ["$scope", function ($scope) {
  }]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('ListadoCtrl', ['$scope', 'usSpinnerService', function($scope, usSpinnerService) { //function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.options = {
      'resource': 'reporte',
      'title': 'Reporte',
      'columns': [
      	{'title': 'Año' , 'data': 'anio'},
		{'title': 'Semana' , 'data' : 'semana'},
		{'title': 'Sexo' , 'data' : 'sexo'},
		{'title': 'ID' , 'data' : 'id', 'visible': false},
		{'title': 'Cantidad' , 'data' : 'cantidad'},
		{'title': 'Mes' , 'data' : 'mes'},
		{'title': 'Día' , 'data' : 'dia'},
		{'title': 'Region' , 'data' : 'region'},
		{'title': 'País' , 'data' : 'pais'},
		{'title': 'Cod. Nivel Adm. 1' , 'data' : 'adm1Codigo', 'visible': false},
		{'title': 'Nivel Adm. 1' , 'data' : 'adm1Nombre'},
		{'title': 'Cod. Nivel Adm. 2' , 'data' : 'adm2Codigo', 'visible': false},
		{'title': 'Nivel Adm. 2' , 'data' : 'adm2Nombre'},
		{'title': 'Cod. Nivel Adm. 3' , 'data' : 'adm3Codigo', 'visible': false},
		{'title': 'Nivel Adm. 3' , 'data': 'adm3Nombre'},
		{'title': 'Sexo' , 'data' : 'sexo'},
		{'title': 'Grupo Edad' , 'data' : 'grupo_edad'},
		{'title': 'Estado Final' , 'data' : 'estado_final'},
		{'title': 'Clasif. clínica' , 'data' : 'clasificacion_clinica'},
		{'title': 'Serotipo' , 'data' : 'serotipo'},
		{'title': 'Fuente' , 'data' : 'fuente', 'visible': false},
		{'title': 'Origen' , 'data' : 'origen', 'visible': false}
      ]
    };

	$scope.startSpinTable = function(){
        usSpinnerService.spin('spinner-table');
    }
    $scope.stopSpinTable = function(){
        usSpinnerService.stop('spinner-table');
    }

    /******* Dowload with filters *******/

    var urlTemplate = window.basePath + "/denguemaps-server/rest/reporte/lista?";
    var tableId = ".table"; //find table by class

  	/* Apply the request for JSON download */
  	$("#btn-descarga-json").on(
      'click',
      function() {
        var oTable = $(tableId).dataTable();
        var oParams = oTable.oApi
            ._fnAjaxParameters(oTable.fnSettings());
        var columnFilters = oParams.columns
        var tieneFiltro = false;
        _.each(columnFilters, function(filter) {
          if (filter.search.value != "")
                tieneFiltro = true;
        });
        if (tieneFiltro) {
        	$scope.startSpinTable();
          	$.ajax({
	            type : 'GET',
	            url : urlTemplate + '?' + $.param(oParams),
	            beforeSend : function(xhr) {
	              xhr.setRequestHeader('Accept', 'application/csv');
	            },
	            success : function(response) {
	              var nameAux = urlTemplate.split("/");
	              var name = nameAux[nameAux.length - 2]; // resource is name
	              download(response, name + ".json", "text/json");
	              $scope.stopSpinTable();
	            },
	            error : function() {
	              console.log("error");
	              $scope.stopSpinTable();
	              alert("Ha ocurrido un error.");
	            }
          	});
        } else {
          alert("Debe indicar al menos un filtro para poder realizar la descarga.");
        }

      });


    /* Apply the request for CSV download */
  	$("#btn-descarga-csv").on(
      'click',
      function() {

        var oTable = $(tableId).dataTable();
        var oParams = oTable.oApi
            ._fnAjaxParameters(oTable.fnSettings());
        var columnFilters = oParams.columns
        var tieneFiltro = false;
        _.each(columnFilters, function(filter) {
          if (filter.search.value != "")
                tieneFiltro = true;
        });
        if (tieneFiltro) {
        	$scope.startSpinTable();
          	$.ajax({
	            type : 'GET',
	            url : urlTemplate + '?' + $.param(oParams),
	            beforeSend : function(xhr) {
	              xhr.setRequestHeader('Accept', 'application/csv');
	            },
	            success : function(response) {
	              var nameAux = urlTemplate.split("/");
	              var name = nameAux[nameAux.length - 2]; //resource is name
	              var JSONData = response;
	              var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
	              var CSV = '';
	              var row = "";
	              // This loop will extract the label from 1st index of on array
	              for ( var index in arrData[0]) {
	                  // Now convert each value to string and comma-seprated
	                  row += index + ',';
	              }
	              row = row.slice(0, -1);
	              // append Label row with line break
	              CSV += row + '\r\n';
	              // 1st loop is to extract each row
	              for (var i = 0; i < arrData.length; i++) {
	                  var row = "";

	                  // 2nd loop will extract each column and convert it in string
	                  // comma-seprated
	                  for ( var index in arrData[i]) {
	                      row += '"' + arrData[i][index] + '",';
	                  }
	                  row.slice(0, row.length - 1);
	                  // add a line break after each row
	                  CSV += row + '\r\n';
	              }

	              if (CSV == '') {
	                  alert("Invalid data");
	                  return;
	              }
	              download(CSV, name + ".csv", "text/csv");
	              $scope.stopSpinTable();
	            },
	            error : function() {
	              console.log("error");
	              $scope.stopSpinTable();
	              alert("Ha ocurrido un error.");
	            }
          	});
        } else {
          alert("Debe indicar al menos un filtro para poder realizar la descarga.");
        }
      });

  }]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AyudaCtrl
 * @description
 * # AyudaCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('AyudaCtrl', ["$scope", function ($scope) {
    $scope.steps;

    $scope.clickHelp = function () {
      console.log("clickHelp");
      //$('#mapa').hasClass('active');
      if ($('#tab-mapa').hasClass('active'))
        $scope.steps = $scope.stepsMapa;
      else if ($('#tab-listado').hasClass('active'))
        $scope.steps = $scope.stepsListado;
      else
        $scope.steps = false;

      introJs().setOptions({
        doneLabel: 'Salir',
        nextLabel: 'Siguiente &rarr;',
        prevLabel: '&larr; Anterior',
        skipLabel: 'Salir',
        steps: $scope.steps
      }).start();
    }

    $scope.stepsMapa = [
    {
      intro: "Bienvenido a esta visualización interactiva.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
      </br></br>Haz click en siguiente para comenzar."
    },
    {
      element: '#tab-mapa',
      intro: "En esta sección, puedes ver los diferentes mapas de Dengue en el Paraguay.",
      position: "right"
    },
    {
      element: '.sidebar-tabs',
      intro: "Puedes seleccionar el tipo de mapa y realizar filtros sobre los resultados.",
      position: 'right'
    },
    {
      element: '.leaflet-control-zoom',
      intro: "Acerca y aleja el mapa con este componente.",
      position: "left"
    },
    {
      element: '.leaflet-control-layers',
      intro: "Cambia el mapa base con este componente.</br></br>¿Te gustaría ver una imagen satelital? Es posible!",
      position: "left"
    },
    {
      element: '.opacity_slider_control',
      intro: "Cambia la opacidad de las capas superpuestas con este componente.",
      position: "left"
    },
    {
      element: '#tab-listado',
      intro: "En la sección de listado, puedes ver datos de las notificaciones de dengue de forma tabular. Visítala!",
      position: "right"
    },
    {
      element: '#tab-diccionario',
      intro: "En la sección de diccionario, puedes ver el diccionario de datos de forma tabular. Visítala!",
      position: "right"
    },
    {
      element: '#tab-datos',
      intro: "En la sección de datos, puedes cargar nuevos datos. Visítala!",
      position: "right"
    },
    {
      element: '#tab-descargas',
      intro: "Haciendo click aquí puedes descargar los datos por año en formato CSV y JSON.",
      position: "right"
    }
    /*{
      element: '.info',
      intro: "Aquí puedes ver los datos del departamento/distrito/barrio visible en el mapa.",
      position: "left"
    },*/
  ];

  $scope.stepsListado = [
    {
      intro: "Bienvenido a esta visualización interactiva.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
      </br></br>Haz click en siguiente para comenzar."
    },
    {
      element: '#tab-listado',
      intro: "En esta sección, puedes ver datos de las notificaciones de dengue de forma tabular.",
      position: "right"
    },
    {
      element: '.dataTables_length',
      intro: "Selecciona la cantidad de filas por página de la tabla.",
      position: 'right'
    },
    {
      element: '.dataTables_filter',
      intro: "Filtra los resultados de acuerdo a los valores de cualquier campo.",
      position: "left"
    },
    {
      element: 'tfoot',
      intro: "O filtra de acuerdo a los valores de una columna en particular.",
      position: "bottom"
    },
    {
      element: '.dataTables_info',
      intro: "Aquí puedes ver un resumen de los resultados de tu búsqueda.",
      position: "right"
    },
    {
      element: '.dataTables_paginate',
      intro: "Si los resultados son muchos, desplázate entre las páginas de la tabla.",
      position: "left"
    },
    {
      element: '#download-footer',
      intro: "Descarga los resultados filtrados en JSON o CSV.",
      position: "top"
    },
    {
      element: '#tab-diccionario',
      intro: "En la sección de diccionario, puedes ver el diccionario de datos de forma tabular. Visítala!",
      position: "right"
    },
    {
      element: '#tab-datos',
      intro: "En la sección de datos, puedes cargar nuevos datos. Visítala!",
      position: "right"
    },
    {
      element: '#tab-descargas',
      intro: "Haciendo click aquí puedes descargar los datos por año en formato CSV y JSON.",
      position: "right"
    },
     {
      element: '#tab-mapa',
      intro: "Por último, ¿ya visitaste la sección del mapa interactivo?",
      position: "right"
    }
  ];

}]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DiccionarioCtrl', ["$scope", function ($scope) {
  }]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:DescargasCtrl
 * @description
 * # DescargasCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DescargasCtrl', ['$scope', 'usSpinnerService', function($scope, usSpinnerService) {
  	var urlBase = window.basePath + "/denguemaps-server/rest/reporte/";

  	$scope.startSpinDropdown = function(){
        usSpinnerService.spin('spinner-dropdown');
    }
    $scope.stopSpinDropdown = function(){
        usSpinnerService.stop('spinner-dropdown');
    }

  	$scope.descargar = function (anio,formato) {
  		if (formato == 'CSV') {
  			descargarCSV(anio);
  		} else {
  			descargarJSON(anio);
  		}
  	};

    function descargarCSV (anio) {
	    $scope.startSpinDropdown();
	    var data;
	    $.ajax({
	        url: urlBase + "descarga/" + anio,
	        type:"get",
	        success: function(response) {
	            var JSONData = response;
	            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
	            var CSV = '';
	            var row = "";
	            // This loop will extract the label from 1st index of on array
	            for ( var index in arrData[0]) {
	                // Now convert each value to string and comma-seprated
	                row += index + ',';
	            }
	            row = row.slice(0, -1);
	            // append Label row with line break
	            CSV += row + '\r\n';
	            // 1st loop is to extract each row
	            for (var i = 0; i < arrData.length; i++) {
	                var row = "";

	                // 2nd loop will extract each column and convert it in string
	                // comma-seprated
	                for ( var index in arrData[i]) {
	                    row += '"' + arrData[i][index] + '",';
	                }
	                row.slice(0, row.length - 1);
	                // add a line break after each row
	                CSV += row + '\r\n';
	            }

	            if (CSV == '') {
	                alert("Invalid data");
	                return;
	            }
	            $scope.stopSpinDropdown();
	            download(CSV, "notificaciones.csv", "text/csv");
	        },
	        error: function(xhr) {
	        	$scope.stopSpinDropdown();
	            console.log('error');
	        }
    	});
	};

	function descargarJSON (anio) {
	    $scope.startSpinDropdown();
	    var data;
	    $.ajax({
	        url: urlBase + "descarga/" + anio,
	        type:"get",
	        success: function(response) {
	            data = response;
	            $scope.stopSpinDropdown();
	            download(JSON.stringify(data, null, 4), "notificaciones.json", "application/json");
	        },
	        error: function(xhr) {
	        	$scope.stopSpinDropdown();
	            console.log('error');
	        }
	    });
	}
  }]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AyudaCtrl
 * @description
 * # AyudaCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('TabsCtrl', ["$scope", "$location", "$rootScope", function ($scope, $location, $rootScope) {
  
    
}]);
'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DatosCtrl', ["$scope", "fileUpload", "toastr", function ($scope, fileUpload, toastr) {

	$scope.uploadFile = function(){
		if ($scope.myFile) {
			console.log($scope.myFile);
	        var file = $scope.myFile;
	        //console.log('file is ' + JSON.stringify(file));
	        var uploadUrl = window.basePath + "/denguemaps-server/rest/file/upload";
	        //var uploadUrl = "http://localhost:8088/municipalidad-app-server/rest/movil/notificacion?latitud=-56.4435053&longitud=-25.7811414&idtipo=1&descripcion=test&identificador=xxx";
	        var result = fileUpload.uploadFileToUrl(file, uploadUrl)
	        	.then(function(data){
	        		console.log(data);
					console.log(data.file);
					switch(data.file){
						case "error_interno":
							$scope.msgUpload = false;
			            	toastr.error('Error interno del sistema','Error');
			            	break;
						case "formato_invalido":
							$scope.msgUpload = false;
			            	toastr.error('El formato del archivo no es válido','Error');
			            	break;
			            case "contenido_de_filas_invalido":
			            	$scope.msgUpload = false;
							toastr.error('El contenido del archivo no es válido','Error');
							break;
						case "estructura_de_columnas_invalida":
							$scope.msgUpload = false;
							toastr.error('La estructura de columnas del archivo no es válida','Error');
							break;
						default:
							$scope.msgUpload = true;
			            	toastr.success('La carga de datos se ha realizado exitosamente');
			            	break;
					}
					return data;
				})
		} else {
			toastr.warning("No se seleccionó ningun archivo.");
		}

    };

    $("#filestyle-7").on('change', function (){
    	var value = $("#filestyle-7").val();
    	var splitValue = value.split('\\');
    	$("#selected-file-value").val(splitValue[splitValue.length - 1]);
    })

  }]);
'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('AuthCtrl', ["$scope", "$rootScope", "$location", "AuthenticationService", function ($scope, $rootScope, $location, AuthenticationService) {

	  	$scope.logout = function () {
	  		//console.log("entro a logout");
		    $scope.dataLoading = true;
		    AuthenticationService.ClearCredentials();
		    /* Falta hacer que se quede en la misma pagina pero que refresque */
		    $location.path('/');
	    }

	    $scope.isLoggedIn = function () {
	  		//console.log("entro a isLoggedIn");
	  		if ($rootScope.globals.currentUser) return false;
		   	else return true;
	    }

}]);
/*
 * Google layer using Google Maps API
 */

/* global google: true */

L.Google = L.Class.extend({
	includes: L.Mixin.Events,

	options: {
		minZoom: 0,
		maxZoom: 18,
		tileSize: 256,
		subdomains: 'abc',
		errorTileUrl: '',
		attribution: '',
		opacity: 1,
		continuousWorld: false,
		noWrap: false,
		mapOptions: {
			backgroundColor: '#dddddd'
		}
	},

	// Possible types: SATELLITE, ROADMAP, HYBRID, TERRAIN
	initialize: function(type, options) {
		L.Util.setOptions(this, options);

		this._ready = google.maps.Map !== undefined;
		if (!this._ready) L.Google.asyncWait.push(this);

		this._type = type || 'SATELLITE';
	},

	onAdd: function(map, insertAtTheBottom) {
		this._map = map;
		this._insertAtTheBottom = insertAtTheBottom;

		// create a container div for tiles
		this._initContainer();
		this._initMapObject();

		// set up events
		map.on('viewreset', this._resetCallback, this);

		this._limitedUpdate = L.Util.limitExecByInterval(this._update, 150, this);
		map.on('move', this._update, this);

		map.on('zoomanim', this._handleZoomAnim, this);

		//20px instead of 1em to avoid a slight overlap with google's attribution
		map._controlCorners.bottomright.style.marginBottom = '20px';

		this._reset();
		this._update();
	},

	onRemove: function(map) {
		map._container.removeChild(this._container);

		map.off('viewreset', this._resetCallback, this);

		map.off('move', this._update, this);

		map.off('zoomanim', this._handleZoomAnim, this);

		map._controlCorners.bottomright.style.marginBottom = '0em';
	},

	getAttribution: function() {
		return this.options.attribution;
	},

	setOpacity: function(opacity) {
		this.options.opacity = opacity;
		if (opacity < 1) {
			L.DomUtil.setOpacity(this._container, opacity);
		}
	},

	setElementSize: function(e, size) {
		e.style.width = size.x + 'px';
		e.style.height = size.y + 'px';
	},

	_initContainer: function() {
		var tilePane = this._map._container,
			first = tilePane.firstChild;

		if (!this._container) {
			this._container = L.DomUtil.create('div', 'leaflet-google-layer leaflet-top leaflet-left');
			this._container.id = '_GMapContainer_' + L.Util.stamp(this);
			this._container.style.zIndex = 'auto';
		}

		tilePane.insertBefore(this._container, first);

		this.setOpacity(this.options.opacity);
		this.setElementSize(this._container, this._map.getSize());
	},

	_initMapObject: function() {
		if (!this._ready) return;
		this._google_center = new google.maps.LatLng(0, 0);
		var map = new google.maps.Map(this._container, {
			center: this._google_center,
			zoom: 0,
			tilt: 0,
			mapTypeId: google.maps.MapTypeId[this._type],
			disableDefaultUI: true,
			keyboardShortcuts: false,
			draggable: false,
			disableDoubleClickZoom: true,
			scrollwheel: false,
			streetViewControl: false,
			styles: this.options.mapOptions.styles,
			backgroundColor: this.options.mapOptions.backgroundColor
		});

		var _this = this;
		this._reposition = google.maps.event.addListenerOnce(map, 'center_changed',
			function() { _this.onReposition(); });
		this._google = map;

		google.maps.event.addListenerOnce(map, 'idle',
			function() { _this._checkZoomLevels(); });
		//Reporting that map-object was initialized.
		this.fire('MapObjectInitialized', { mapObject: map });
	},

	_checkZoomLevels: function() {
		//setting the zoom level on the Google map may result in a different zoom level than the one requested
		//(it won't go beyond the level for which they have data).
		// verify and make sure the zoom levels on both Leaflet and Google maps are consistent
		if (this._google.getZoom() !== this._map.getZoom()) {
			//zoom levels are out of sync. Set the leaflet zoom level to match the google one
			this._map.setZoom( this._google.getZoom() );
		}
	},

	_resetCallback: function(e) {
		this._reset(e.hard);
	},

	_reset: function(clearOldContainer) {
		this._initContainer();
	},

	_update: function(e) {
		if (!this._google) return;
		this._resize();

		var center = this._map.getCenter();
		var _center = new google.maps.LatLng(center.lat, center.lng);

		this._google.setCenter(_center);
		this._google.setZoom(Math.round(this._map.getZoom()));

		this._checkZoomLevels();
	},

	_resize: function() {
		var size = this._map.getSize();
		if (this._container.style.width === size.x &&
				this._container.style.height === size.y)
			return;
		this.setElementSize(this._container, size);
		this.onReposition();
	},


	_handleZoomAnim: function (e) {
		var center = e.center;
		var _center = new google.maps.LatLng(center.lat, center.lng);

		this._google.setCenter(_center);
		this._google.setZoom(Math.round(e.zoom));
	},


	onReposition: function() {
		if (!this._google) return;
		google.maps.event.trigger(this._google, 'resize');
	}
});

L.Google.asyncWait = [];
L.Google.asyncInitialize = function() {
	var i;
	for (i = 0; i < L.Google.asyncWait.length; i++) {
		var o = L.Google.asyncWait[i];
		o._ready = true;
		if (o._container) {
			o._initMapObject();
			o._update();
		}
	}
	L.Google.asyncWait = [];
};

'use strict';

/**
 * @ngdoc directive
 * @name coreApp.directive:genericDatatable
 * @description
 * # genericDatatable
 */
angular.module('denguemapsApp')
  .directive('genericDatatable', ["$resource", "$location", "DTOptionsBuilder", "DTColumnBuilder", function ($resource, $location, DTOptionsBuilder, DTColumnBuilder) {
    return {
      templateUrl: 'views/templates/generic-datatable.html',
      restrict: 'AE',
      replace: true,
      scope: {
        options: '='
      },
      controller: ["$scope", "$element", function postLink($scope, $element) {
        var urlTemplate = _.template(window.basePath + '/denguemaps-server/rest/<%= resource %>/lista?');
        var format = $scope.options.format || 'application/json';
        $scope.dtInstance = {};
        var ajaxRequest = function(data, callback) {
          // TODO: mandar esto al widget de datatable generico
          var xhr = $resource(urlTemplate($scope.options) + $.param(data), {}, {
            query: {
              isArray: false
            },
            get: {
              method: 'GET',
              headers: { 'Accept': format }
            }
          });
          xhr.query().$promise.then(function(response) {
            callback(response);
          });
        };
        var ajaxConfig = ($scope.options.ajax) ? $scope.options.ajax : ajaxRequest;

        $scope.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('ajax', ajaxConfig)
          .withDataProp('data')
          .withOption('processing', true)
          .withOption('serverSide', true)
          .withOption('language', {
                  "sProcessing" : "Procesando...",
                  "sLengthMenu" : "Mostrar _MENU_ registros",
                  "sZeroRecords" : "No se encontraron resultados",
                  "sEmptyTable" : "Ningún dato disponible en esta tabla",
                  "sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix" : "",
                  "sSearch" : "Buscar:",
                  "sUrl" : "",
                  "sInfoThousands" : ",",
                  "sLoadingRecords" : "Cargando...",
                  "oPaginate" : {
                    "sFirst" : "Primero",
                    "sLast" : "Último",
                    "sNext" : "Siguiente",
                    "sPrevious" : "Anterior"
                  },
                  "oAria" : {
                    "sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending" : ": Activar para ordenar la columna de manera descendente"
                  }
                })
          .withPaginationType('full_numbers')
          .withBootstrap();

        $scope.visibleColumns = $scope.options.columns.length;

        $scope.dtColumns = _.map($scope.options.columns, function(c){
          var column = DTColumnBuilder.newColumn(c.data);
          var commonAttrs = ['data', 'title', 'class', 'renderWith', 'visible', 'sortable']
          if(c.title) column = column.withTitle(c.title);
          if(c.class) column = column.withClass(c.class);
          if(c.renderWith) column = column = column.renderWith(c.renderWith);
          if(c.visible === false) {
            column = column.notVisible();
            $scope.visibleColumns -= 1;
          }
          if(c.sortable === false) column = column.notSortable();
          _.forOwn(c, function(value, key){
            if(!_.contains(commonAttrs, key)){
              column = column.withOption(key, value);
            }
          });
          return column;
        });

        /*$scope.new = function(){
          var pathTemplate = _.template('/<%= resource %>/new');
          $location.path(pathTemplate($scope.options));
        }*/

        /* FALTA ver como agregar los filtros solo segun la cantidad de columnas visibles */
        $scope.dtInstanceCallback = function(dtInstance){
          console.log($scope.dtVisibleColumns);
          var tableId = dtInstance.id;

          for (var i = 0; i < $scope.visibleColumns; i++) {
            $('#' + tableId + ' tfoot tr').append('<th></th>');
          }

          // Setup - add a text input to each footer cell
          $('#' + tableId + ' tfoot th').each(
            function() {
              var title = $('#' + tableId + ' thead th').eq($(this).index()).text();
              $(this).html(
                  '<input class="column-filter form-control input-sm" type="text" placeholder="'
                      + title + '" style="width: 100%;" />');
          });

          $('tfoot').insertAfter('thead');
          var table = dtInstance.DataTable;
          table.columns().eq(0).each(
            function(colIdx) {
              $('tfoot input:eq(' + colIdx.toString() + ')').on('keyup change',
                  function(e) {
                      var realIndex;
                      var that = this;
                      _.each($scope.dtColumns, function(object, index) {
                          if (object.sTitle == that.placeholder) {
                              realIndex = index;
                          }
                      });
                      var index = realIndex || colIdx;
                      if(this.value.length >= 1 || e.keyCode == 13){
                        table.column(index).search(this.value).draw();
                      }
                      // Ensure we clear the search if they backspace far enough
                      if(this.value == "") {
                          table.column(index).search("").draw();
                      }
                  });
          });
        }
      }]
    };
  }]);

'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AyudaCtrl
 * @description
 * # AyudaCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DropdownCtrl', ["$scope", "$log", function ($scope, $log) {
    $scope.years = ['2009','2010','2011','2012','2013', '2014', '2015'];
    $scope.types = ['JSON','CSV'];

    $scope.status = {
      isopen: false
    };

    $scope.toggled = function(open) {
      $log.log('Dropdown is now: ', open);
    };

    $scope.toggleDropdown = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.isopen = !$scope.status.isopen;
    };
}]);
//download.js v3.1, by dandavis; 2008-2014. [CCBY2] see http://danml.com/download.html for tests/usage
// v1 landed a FF+Chrome compat way of downloading strings to local un-named files, upgraded to use a hidden frame and optional mime
// v2 added named files via a[download], msSaveBlob, IE (10+) support, and window.URL support for larger+faster saves than dataURLs
// v3 added dataURL and Blob Input, bind-toggle arity, and legacy dataURL fallback was improved with force-download mime and base64 support. 3.1 improved safari handling.

// https://github.com/rndme/download

// data can be a string, Blob, File, or dataURL
function download(data, strFileName, strMimeType) {
	
	var self = window, // this script is only for browsers anyway...
		u = "application/octet-stream", // this default mime also triggers iframe downloads
		m = strMimeType || u, 
		x = data,
		D = document,
		a = D.createElement("a"),
		z = function(a){return String(a);},
		B = (self.Blob || self.MozBlob || self.WebKitBlob || z);
		B=B.call ? B.bind(self) : Blob ;
		var fn = strFileName || "download",
		blob, 
		fr;

	
	if(String(this)==="true"){ //reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
		x=[x, m];
		m=x[0];
		x=x[1]; 
	}
	
	


	//go ahead and download dataURLs right away
	if(String(x).match(/^data\:[\w+\-]+\/[\w+\-]+[,;]/)){
		return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
			navigator.msSaveBlob(d2b(x), fn) : 
			saver(x) ; // everyone else can save dataURLs un-processed
	}//end if dataURL passed?
	
	blob = x instanceof B ? 
		x : 
		new B([x], {type: m}) ;
	
	
	function d2b(u) {
		var p= u.split(/[:;,]/),
		t= p[1],
		dec= p[2] == "base64" ? atob : decodeURIComponent,
		bin= dec(p.pop()),
		mx= bin.length,
		i= 0,
		uia= new Uint8Array(mx);

		for(i;i<mx;++i) uia[i]= bin.charCodeAt(i);

		return new B([uia], {type: t});
	 }
	  
	function saver(url, winMode){
		
		if ('download' in a) { //html5 A[download] 			
			a.href = url;
			a.setAttribute("download", fn);
			a.innerHTML = "downloading...";
			D.body.appendChild(a);
			setTimeout(function() {
				a.click();
				D.body.removeChild(a);
				if(winMode===true){setTimeout(function(){ self.URL.revokeObjectURL(a.href);}, 250 );}
			}, 66);
			return true;
		}

		if(typeof safari !=="undefined" ){ // handle non-a[download] safari as best we can:
			url="data:"+url.replace(/^data:([\w\/\-\+]+)/, u);
			if(!window.open(url)){ // popup blocked, offer direct download: 
				if(confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")){ location.href=url; }
			}
			return true;
		}
		
		//do iframe dataURL download (old ch+FF):
		var f = D.createElement("iframe");
		D.body.appendChild(f);
		
		if(!winMode){ // force a mime that will download:
			url="data:"+url.replace(/^data:([\w\/\-\+]+)/, u);
		}
		f.src=url;
		setTimeout(function(){ D.body.removeChild(f); }, 333);
		
	}//end saver 
		



	if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
		return navigator.msSaveBlob(blob, fn);
	} 	
	
	if(self.URL){ // simple fast and modern way using Blob and URL:
		saver(self.URL.createObjectURL(blob), true);
	}else{
		// handle non-Blob()+non-URL browsers:
		if(typeof blob === "string" || blob.constructor===z ){
			try{
				return saver( "data:" +  m   + ";base64,"  +  self.btoa(blob)  ); 
			}catch(y){
				return saver( "data:" +  m   + "," + encodeURIComponent(blob)  ); 
			}
		}
		
		// Blob but not URL:
		fr=new FileReader();
		fr.onload=function(e){
			saver(this.result); 
		};
		fr.readAsDataURL(blob);
	}	
	return true;
} /* end download() */
'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:panelSlider
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('panelSlider', ["$rootScope", "$http", "mapUtil", function ($rootScope,  $http, mapUtil) {
    return {
		restrict: 'E',
		replace: false,
		scope: {
			detalle:'='
		},
        templateUrl: 'views/templates/panel-slider.html',
    	link: function postLink(scope, element, attrs) {
			
			var mapSlider;
			var selectedYear = 2013;
			var selectedWeek = 1;

			$("#sYear").noUiSlider({
				start: 2013,
				direction: "ltr",
				step: 1,
				range: {
					'min': 2009,
					'max': 2013
				},
				format: wNumb({
					decimals: 0
				})
			});
			$('#sYear').noUiSlider_pips({
				mode: 'steps',
				density: 4
			});

			$("#sMonth").noUiSlider({
				start: 25,
				step: 1,
				direction: "ltr",
				range: {
					'min': 1,
					'max': 53
				},
				format: wNumb({
					decimals: 0
				})
			});
			$('#sMonth').noUiSlider_pips({
				mode: 'count',
				values: 6,
				density: 4
			});


			$("#sYear").Link('lower').to($("#fYear"));
			$("#sMonth").Link('lower').to($("#fMonth"));

			/*$("#sMonth").on({
				change: function(){
					mapUtil.reloadSem($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					mapUtil.reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});*/

	    }
	};
}]);
'use strict';

angular.module('denguemapsApp')
  .directive("loginForm", [ function () {
      return {
        restrict: 'E',
        scope: {},
        template: '<div class="row"> <div class="col-xs-12"> <form name="loginForm"> <div class="form-group"> <label for="username">Username</label> <input type="text" name="username" ng-model="username" required="required" class="form-control"/> </div> <div class="form-group"> <label for="password">Password</label> <input type="password" name="password" ng-model="password" required="required" class="form-control"/> </div> <button type="submit" ng-disabled="!loginForm.$valid" ng-class="{\'btn-default\':!loginForm.$valid,\'btn-success\':loginForm.$valid}" ng-click="login()" class="btn login">Login</button> <span ng-show="loginError && !loginForm.$valid" class="label label-danger"> <b>Error With Login</b> Please try again. </span></form> </div> </div>',
        replace: 'true',
        controller: ['$scope', '$http', '$window', '$rootScope', '$location', 'AuthenticationService',
          function($scope, $http, $window, $rootScope, $location, AuthenticationService) {

            AuthenticationService.ClearCredentials();

            $scope.login = function () {
              $scope.dataLoading = true;
              AuthenticationService.Login($scope.username, $scope.password, function(response) {
                console.log(response);
                if(response) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/datos');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                    alert("No se ha podido autenticar.");
                }
              });
            };

          }]
      }
    }])
'use strict';

angular.module('denguemapsApp').factory('httpInterceptor', ["$q", "$window", "$location", function httpInterceptor ($q, $window, $location) {
  return function (promise) {
      var success = function (response) {
          return response;
      };

      var error = function (response) {
          if (response.status === 401) {
              $location.url('/login');
          }

          return $q.reject(response);
      };

      return promise.then(success, error);
  };
}]);
'use strict';

angular.module('denguemapsApp').factory('AuthenticationService',
  ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout', 'CORSService',
    function (Base64, $http, $cookieStore, $rootScope, $timeout, CORSService) {
        var service = {};

        service.Login = function (username, password, callback) {
 			console.log("authentication");
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/
            /*$timeout(function(){
                var response = { success: username === 'test' && password === 'test' };
                if(!response.success) {
                    response.message = 'Username or password is incorrect';
                }
                callback(response);
            }, 1000);*/

            var url = window.basePath + '/denguemaps-server/rest/auth/login';
            /* Use this for real authentication
             ----------------------------------------------*/
            $http.post(url, { username: username, pwd: password })
                .success(function (response) {
                    callback(response);
                });

            //CORSService.makeCorsRequest('POST', url, { username: username, password: password }, callback);

        };

        service.SetCredentials = function (username, password) {
            console.log("set credentials");
            var authdata = Base64.encode(username + ':' + password);

            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        };

        service.ClearCredentials = function () {
            console.log("clear credentials")
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        };

        return service;
    }])

.factory('Base64', function () {
    /* jshint ignore:start */

    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };
});
'use strict';

angular.module('denguemapsApp').factory('CORSService', function () {
        var cors = {};

        // Create the XHR object.
        cors.createCORSRequest = function (method, url) {
          console.log("createCORSRequest");
          var xhr = new XMLHttpRequest();
          if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari.
            xhr.open(method, url, true);
          } else if (typeof XDomainRequest != "undefined") {
            // XDomainRequest for IE.
            xhr = new XDomainRequest();
            xhr.open(method, url);
          } else {
            // CORS not supported.
            xhr = null;
          }
          return xhr;
        }

        // Make the actual CORS request.
        cors.makeCorsRequest = function (method, url, data, callback) {
          console.log("makeCorsRequest");
          // All HTML5 Rocks properties support CORS.

          var xhr = cors.createCORSRequest(method, url);
          if (!xhr) {
            console.log('CORS not supported');
            return;
          }
          // Response handlers.
          xhr.onload = function (response) {
            console.log('Response from CORS request to ' + url);
            callback(xhr);
          }

          xhr.onerror = function() {
            console.log('Woops, there was an error making the request.');
            callback(xhr);
          };

          xhr.send();
        }

        return cors;
    })
/*
    D3.js Slider
    Inspired by jQuery UI Slider
    Copyright (c) 2013, Bjorn Sandvik - http://blog.thematicmapping.org
    BSD license: http://opensource.org/licenses/BSD-3-Clause
*/

d3.slider = function module() {
  "use strict";

  // Public variables width default settings
  var min = 0,
      max = 100,
      step = 0.01,
      animate = true,
      orientation = "horizontal",
      axis = false,
      margin = 50,
      value,
      active = 1,
      scale;

  // Private variables
  var axisScale,
      dispatch = d3.dispatch("slide", "slideend"),
      formatPercent = d3.format(".2%"),
      tickFormat = d3.format(".0"),
      sliderLength;

  function slider(selection) {
    selection.each(function() {

      // Create scale if not defined by user
      if (!scale) {
        scale = d3.scale.linear().domain([min, max]);
      }

      // Start value
      value = value || scale.domain()[0];

      // DIV container
      var div = d3.select(this).classed("d3-slider d3-slider-" + orientation, true);

      var drag = d3.behavior.drag();
      drag.on('dragend', function () {
        dispatch.slideend(d3.event, value);
      })

      // Slider handle
      //if range slider, create two
      var handle1, handle2 = null, divRange;

      if ( value.length == 2 ) {
        handle1 = div.append("a")
          .classed("d3-slider-handle", true)
          .attr("xlink:href", "#")
          .attr('id', "handle-one")
          .on("click", stopPropagation)
          .call(drag);
        handle2 = div.append("a")
          .classed("d3-slider-handle", true)
          .attr('id', "handle-two")
          .attr("xlink:href", "#")
          .on("click", stopPropagation)
          .call(drag);
      } else {
        handle1 = div.append("a")
          .classed("d3-slider-handle", true)
          .attr("xlink:href", "#")
          .attr('id', "handle-one")
          .on("click", stopPropagation)
          .call(drag);
      }

      // Horizontal slider
      if (orientation === "horizontal") {

        div.on("click", onClickHorizontal);

        if ( value.length == 2 ) {
          divRange = d3.select(this).append('div').classed("d3-slider-range", true);

          handle1.style("left", formatPercent(scale(value[ 0 ])));
          divRange.style("left", formatPercent(scale(value[ 0 ])));
          drag.on("drag", onDragHorizontal);

          var width = 100 - parseFloat(formatPercent(scale(value[ 1 ])));
          handle2.style("left", formatPercent(scale(value[ 1 ])));
          divRange.style("right", width+"%");
          drag.on("drag", onDragHorizontal);

        } else {
          handle1.style("left", formatPercent(scale(value)));
          drag.on("drag", onDragHorizontal);
        }

        sliderLength = parseInt(div.style("width"), 10);

      } else { // Vertical

        div.on("click", onClickVertical);
        drag.on("drag", onDragVertical);
        if ( value.length == 2 ) {
          divRange = d3.select(this).append('div').classed("d3-slider-range-vertical", true);

          handle1.style("bottom", formatPercent(scale(value[ 0 ])));
          divRange.style("bottom", formatPercent(scale(value[ 0 ])));
          drag.on("drag", onDragVertical);

          var top = 100 - parseFloat(formatPercent(scale(value[ 1 ])));
          handle2.style("bottom", formatPercent(scale(value[ 1 ])));
          divRange.style("top", top+"%");
          drag.on("drag", onDragVertical);

        } else {
          handle1.style("bottom", formatPercent(scale(value)));
          drag.on("drag", onDragVertical);
        }

        sliderLength = parseInt(div.style("height"), 10);

      }

      if (axis) {
        createAxis(div);
      }


      function createAxis(dom) {

        // Create axis if not defined by user
        if (typeof axis === "boolean") {

          axis = d3.svg.axis()
              .ticks(Math.round(sliderLength / 100))
              .tickFormat(tickFormat)
              .orient((orientation === "horizontal") ? "bottom" :  "right");

        }

        // Copy slider scale to move from percentages to pixels
        axisScale = scale.copy().range([0, sliderLength]);
          axis.scale(axisScale);

          // Create SVG axis container
        var svg = dom.append("svg")
            .classed("d3-slider-axis d3-slider-axis-" + axis.orient(), true)
            .on("click", stopPropagation);

        var g = svg.append("g");

        // Horizontal axis
        if (orientation === "horizontal") {

          svg.style("left", -margin + "px");

          svg.attr({
            width: sliderLength + margin * 2,
            height: margin
          });

          if (axis.orient() === "top") {
            svg.style("top", -margin + "px");
            g.attr("transform", "translate(" + margin + "," + margin + ")");
          } else { // bottom
            g.attr("transform", "translate(" + margin + ",0)");
          }

        } else { // Vertical

          svg.style("top", -margin + "px");

          svg.attr({
            width: margin,
            height: sliderLength + margin * 2
          });

          if (axis.orient() === "left") {
            svg.style("left", -margin + "px");
            g.attr("transform", "translate(" + margin + "," + margin + ")");
          } else { // right
            g.attr("transform", "translate(" + 0 + "," + margin + ")");
          }

        }

        g.call(axis);

      }


      // Move slider handle on click/drag
      function moveHandle(pos) {

        var newValue = stepValue(scale.invert(pos / sliderLength)),
            currentValue = value.length ? value[active - 1]: value;

        if (currentValue !== newValue) {
          var oldPos = formatPercent(scale(stepValue(currentValue))),
              newPos = formatPercent(scale(stepValue(newValue))),
              position = (orientation === "horizontal") ? "left" : "bottom";

          if ( value.length === 2) {
            value[ active - 1 ] = newValue;
            dispatch.slide(d3.event, value );
          } else {
            dispatch.slide(d3.event.sourceEvent || d3.event, value = newValue);
          }

          if ( value[ 0 ] >= value[ 1 ] ) return;
          if ( active === 1 ) {

            if (value.length === 2) {
              (position === "left") ? divRange.style("left", newPos) : divRange.style("bottom", newPos);
            }

            if (animate) {
              handle1.transition()
                  .styleTween(position, function() { return d3.interpolate(oldPos, newPos); })
                  .duration((typeof animate === "number") ? animate : 250);
            } else {
              handle1.style(position, newPos);
            }
          } else {

            var width = 100 - parseFloat(newPos);
            var top = 100 - parseFloat(newPos);

            (position === "left") ? divRange.style("right", width + "%") : divRange.style("top", top + "%");

            if (animate) {
              handle2.transition()
                  .styleTween(position, function() { return d3.interpolate(oldPos, newPos); })
                  .duration((typeof animate === "number") ? animate : 250);
            } else {
              handle2.style(position, newPos);
            }
          }
        }

      }


      // Calculate nearest step value
      function stepValue(val) {

        if (val === scale.domain()[0] || val === scale.domain()[1]) {
          return val;
        }

        var valModStep = (val - scale.domain()[0]) % step,
            alignValue = val - valModStep;

        if (Math.abs(valModStep) * 2 >= step) {
          alignValue += (valModStep > 0) ? step : -step;
        }

        return alignValue;

      }


      function onClickHorizontal() {
        if (!value.length) {
          moveHandle(d3.event.offsetX || d3.event.layerX);
        }
      }

      function onClickVertical() {
        if (!value.length) {
          moveHandle(sliderLength - d3.event.offsetY || d3.event.layerY);
        }
      }

      function onDragHorizontal() {
        if ( d3.event.sourceEvent.target.id === "handle-one") {
          active = 1;
        } else if ( d3.event.sourceEvent.target.id == "handle-two" ) {
          active = 2;
        }
        moveHandle(Math.max(0, Math.min(sliderLength, d3.event.x)));
      }

      function onDragVertical() {
        if ( d3.event.sourceEvent.target.id === "handle-one") {
          active = 1;
        } else if ( d3.event.sourceEvent.target.id == "handle-two" ) {
          active = 2;
        }
        moveHandle(sliderLength - Math.max(0, Math.min(sliderLength, d3.event.y)));
      }

      function stopPropagation() {
        d3.event.stopPropagation();
      }

    });

  }

  // Getter/setter functions
  slider.min = function(_) {
    if (!arguments.length) return min;
    min = _;
    return slider;
  };

  slider.max = function(_) {
    if (!arguments.length) return max;
    max = _;
    return slider;
  };

  slider.step = function(_) {
    if (!arguments.length) return step;
    step = _;
    return slider;
  };

  slider.animate = function(_) {
    if (!arguments.length) return animate;
    animate = _;
    return slider;
  };

  slider.orientation = function(_) {
    if (!arguments.length) return orientation;
    orientation = _;
    return slider;
  };

  slider.axis = function(_) {
    if (!arguments.length) return axis;
    axis = _;
    return slider;
  };

  slider.margin = function(_) {
    if (!arguments.length) return margin;
    margin = _;
    return slider;
  };

  slider.value = function(_) {
    if (!arguments.length) return value;
    value = _;
    return slider;
  };

  slider.scale = function(_) {
    if (!arguments.length) return scale;
    scale = _;
    return slider;
  };

  d3.rebind(slider, dispatch, "on");

  return slider;

};



L.Control.Sidebar = L.Control.extend({

    includes: L.Mixin.Events,

    options: {
        closeButton: true,
        position: 'left',
        autoPan: true,
    },

    initialize: function (placeholder, options) {
        L.setOptions(this, options);

        // Find content container
        var content = this._contentContainer = L.DomUtil.get(placeholder);

        // Remove the content container from its original parent
        content.parentNode.removeChild(content);

        var l = 'leaflet-';

        // Create sidebar container
        var container = this._container =
            L.DomUtil.create('div', l + 'sidebar ' + this.options.position);

        // Style and attach content container
        L.DomUtil.addClass(content, l + 'control');
        container.appendChild(content);

        // Create close button and attach it if configured
        if (this.options.closeButton) {
            var close = this._closeButton =
                L.DomUtil.create('a', 'close', container);
            close.innerHTML = '&times;';
        }
    },

    addTo: function (map) {
        var container = this._container;
        var content = this._contentContainer;

        // Attach event to close button
        if (this.options.closeButton) {
            var close = this._closeButton;

            L.DomEvent.on(close, 'click', this.hide, this);
        }

        L.DomEvent
            .on(container, 'transitionend',
                this._handleTransitionEvent, this)
            .on(container, 'webkitTransitionEnd',
                this._handleTransitionEvent, this);

        // Attach sidebar container to controls container
        var controlContainer = map._controlContainer;
        controlContainer.insertBefore(container, controlContainer.firstChild);

        this._map = map;

        // Make sure we don't drag the map when we interact with the content
        var stop = L.DomEvent.stopPropagation;
        L.DomEvent
            .on(content, 'click', stop)
            .on(content, 'mousedown', stop)
            .on(content, 'touchstart', stop)
            .on(content, 'dblclick', stop)
            .on(content, 'mousewheel', stop)
            .on(content, 'MozMousePixelScroll', stop);

        return this;
    },

    removeFrom: function (map) {
        //if the control is visible, hide it before removing it.
        this.hide();

        var content = this._contentContainer;

        // Remove sidebar container from controls container
        var controlContainer = map._controlContainer;
        controlContainer.removeChild(this._container);

        //disassociate the map object
        this._map = null;

        // Unregister events to prevent memory leak
        var stop = L.DomEvent.stopPropagation;
        L.DomEvent
            .off(content, 'click', stop)
            .off(content, 'mousedown', stop)
            .off(content, 'touchstart', stop)
            .off(content, 'dblclick', stop)
            .off(content, 'mousewheel', stop)
            .off(content, 'MozMousePixelScroll', stop);

        L.DomEvent
            .off(container, 'transitionend',
                this._handleTransitionEvent, this)
            .off(container, 'webkitTransitionEnd',
                this._handleTransitionEvent, this);

        if (this._closeButton && this._close) {
            var close = this._closeButton;

            L.DomEvent.off(close, 'click', this.hide, this);
        }

        return this;
    },

    isVisible: function () {
        return L.DomUtil.hasClass(this._container, 'visible');
    },

    show: function () {
        if (!this.isVisible()) {
            L.DomUtil.addClass(this._container, 'visible');
            if (this.options.autoPan) {
                this._map.panBy([-this.getOffset() / 2, 0], {
                    duration: 0.5
                });
            }
            this.fire('show');
        }
    },

    hide: function (e) {
        if (this.isVisible()) {
            L.DomUtil.removeClass(this._container, 'visible');
            if (this.options.autoPan) {
                this._map.panBy([this.getOffset() / 2, 0], {
                    duration: 0.5
                });
            }
            this.fire('hide');
        }
        if(e) {
            L.DomEvent.stopPropagation(e);
        }
    },

    toggle: function () {
        if (this.isVisible()) {
            this.hide();
        } else {
            this.show();
        }
    },

    getContainer: function () {
        return this._contentContainer;
    },

    getCloseButton: function () {
        return this._closeButton;
    },

    setContent: function (content) {
        this.getContainer().innerHTML = content;
        return this;
    },

    getOffset: function () {
        if (this.options.position === 'right') {
            return -this._container.offsetWidth;
        } else {
            return this._container.offsetWidth;
        }
    },

    _handleTransitionEvent: function (e) {
        if (e.propertyName == 'left' || e.propertyName == 'right')
            this.fire(this.isVisible() ? 'shown' : 'hidden');
    }
});

L.control.sidebar = function (placeholder, options) {
    return new L.Control.Sidebar(placeholder, options);
};

/*
        Leaflet.OpacityControls, a plugin for adjusting the opacity of a Leaflet map.
        (c) 2013, Jared Dominguez
        (c) 2013, LizardTech

        https://github.com/lizardtechblog/Leaflet.OpacityControls
*/

//Declare global variables
var opacity_layer;

//Create a control to increase the opacity value. This makes the image more opaque.
L.Control.higherOpacity = L.Control.extend({
    options: {
        position: 'topright'
    },
    setOpacityLayer: function (layer) {
            opacity_layer = layer;
    },
    onAdd: function () {
        
        var higher_opacity_div = L.DomUtil.create('div', 'higher_opacity_control');

        L.DomEvent.addListener(higher_opacity_div, 'click', L.DomEvent.stopPropagation)
            .addListener(higher_opacity_div, 'click', L.DomEvent.preventDefault)
            .addListener(higher_opacity_div, 'click', function () { onClickHigherOpacity() });
        
        return higher_opacity_div;
    }
});

//Create a control to decrease the opacity value. This makes the image more transparent.
L.Control.lowerOpacity = L.Control.extend({
    options: {
        position: 'topright'
    },
    setOpacityLayer: function (layer) {
            opacity_layer = layer;
    },
    onAdd: function (map) {
        
        var lower_opacity_div = L.DomUtil.create('div', 'lower_opacity_control');

        L.DomEvent.addListener(lower_opacity_div, 'click', L.DomEvent.stopPropagation)
            .addListener(lower_opacity_div, 'click', L.DomEvent.preventDefault)
            .addListener(lower_opacity_div, 'click', function () { onClickLowerOpacity() });
        
        return lower_opacity_div;
    }
});

//Create a jquery-ui slider with values from 0 to 100. Match the opacity value to the slider value divided by 100.
L.Control.opacitySlider = L.Control.extend({
    options: {
        position: 'topright'
    },
    setOpacityLayer: function (layer) {
            opacity_layer = layer;
    },
    onAdd: function (map) {
        var opacity_slider_div = L.DomUtil.create('div', 'opacity_slider_control');
        
        $(opacity_slider_div).slider({
          orientation: "vertical",
          range: "min",
          min: 0,
          max: 100,
          value: 60,
          step: 10,
          start: function ( event, ui) {
            //When moving the slider, disable panning.
            map.dragging.disable();
            map.once('mousedown', function (e) { 
              map.dragging.enable();
            });
          },
          slide: function ( event, ui ) {
            var slider_value = ui.value / 100;
            opacity_layer.setOpacity(slider_value);
          }
        });
        
        return opacity_slider_div;
    }
});


function onClickHigherOpacity() {
    var opacity_value = opacity_layer.options.opacity;
    
    if (opacity_value > 1) {
        return;
    } else {
        opacity_layer.setOpacity(opacity_value + 0.2);
        //When you double-click on the control, do not zoom.
        map.doubleClickZoom.disable();
        map.once('click', function (e) { 
            map.doubleClickZoom.enable();
        });
    }

}

function onClickLowerOpacity() {
    var opacity_value = opacity_layer.options.opacity;
    
    if (opacity_value < 0) {
        return;
    } else {
        opacity_layer.setOpacity(opacity_value - 0.2);
        //When you double-click on the control, do not zoom.
        map.doubleClickZoom.disable();
        map.once('click', function (e) { 
            map.doubleClickZoom.enable();
        });
    }
      
}


L.Control.Sidebar = L.Control.extend({
    includes: L.Mixin.Events,

    initialize: function (id, options) {
        var i, child;

        L.setOptions(this, options);

        // Find sidebar HTMLElement
        this._sidebar = L.DomUtil.get(id);

        // Attach touch styling if necessary
        if (L.Browser.touch)
            L.DomUtil.addClass(this._sidebar, 'leaflet-touch');

        // Find sidebar > ul.sidebar-tabs and sidebar > div.sidebar-content
        for (i = this._sidebar.children.length - 1; i >= 0; i--) {
            child = this._sidebar.children[i];
            if (child.tagName == 'UL' &&
                    L.DomUtil.hasClass(child, 'sidebar-tabs'))
                this._tabs = child;

            else if (child.tagName == 'DIV' &&
                    L.DomUtil.hasClass(child, 'sidebar-content'))
                this._container = child;
        }

        // Find sidebar > ul.sidebar-tabs > li
        this._tabitems = [];
        for (i = this._tabs.children.length - 1; i >= 0; i--) {
            child = this._tabs.children[i];
            if (child.tagName == 'LI') {
                this._tabitems.push(child);
                child._sidebar = this;
            }
        }

        // Find sidebar > div.sidebar-content > div.sidebar-pane
        this._panes = [];
        for (i = this._container.children.length - 1; i >= 0; i--) {
            child = this._container.children[i];
            if (child.tagName == 'DIV' &&
                L.DomUtil.hasClass(child, 'sidebar-pane'))
                this._panes.push(child);
        }

        this._hasTouchStart = L.Browser.touch &&
            ('ontouchstart' in document.documentElement);
    },

    addTo: function (map) {
        this._map = map;

        //var e = this._hasTouchStart ? 'touchstart' : 'click';
        for (var i = this._tabitems.length - 1; i >= 0; i--) {
            var child = this._tabitems[i];
            //L.DomEvent.on(child.firstChild, e, this._onClick, child);
            L.DomEvent.on(child.firstChild, 'click', this._onClick, child);
            if(this._hasTouchStart)
              L.DomEvent.on(child.firstChild, 'touchstart', this._onClick, child);
        }

        return this;
    },

    removeFrom: function (map) {
        this._map = null;

        //var e = this._hasTouchStart ? 'touchstart' : 'click';
        for (var i = this._tabitems.length - 1; i >= 0; i--) {
            var child = this._tabitems[i];
            //L.DomEvent.off(child.firstChild, e, this._onClick);
            L.DomEvent.on(child.firstChild, 'click', this._onClick, child);
            if(this._hasTouchStart)
              L.DomEvent.on(child.firstChild, 'touchstart', this._onClick, child);
        }

        return this;
    },

    open: function(id) {
        var i, child;

        // hide old active contents and show new content
        for (i = this._panes.length - 1; i >= 0; i--) {
            child = this._panes[i];
            if (child.id == id)
                L.DomUtil.addClass(child, 'active');
            else if (L.DomUtil.hasClass(child, 'active'))
                L.DomUtil.removeClass(child, 'active');
        }

        // remove old active highlights and set new highlight
        for (i = this._tabitems.length - 1; i >= 0; i--) {
            child = this._tabitems[i];
            if (child.firstChild.hash == '#' + id)
                L.DomUtil.addClass(child, 'active');
            else if (L.DomUtil.hasClass(child, 'active'))
                L.DomUtil.removeClass(child, 'active');
        }

        this.fire('content', { id: id });

        // open sidebar (if necessary)
        if (L.DomUtil.hasClass(this._sidebar, 'collapsed')) {
            this.fire('opening');
            L.DomUtil.removeClass(this._sidebar, 'collapsed');
        }

        return this;
    },

    close: function() {
        // remove old active highlights
        for (var i = this._tabitems.length - 1; i >= 0; i--) {
            var child = this._tabitems[i];
            if (L.DomUtil.hasClass(child, 'active'))
                L.DomUtil.removeClass(child, 'active');
        }

        // close sidebar
        if (!L.DomUtil.hasClass(this._sidebar, 'collapsed')) {
            this.fire('closing');
            L.DomUtil.addClass(this._sidebar, 'collapsed');
        }

        return this;
    },

    _onClick: function(e) {
        e.preventDefault();
        if (L.DomUtil.hasClass(this, 'active'))
            this._sidebar.close();
        else
            this._sidebar.open(this.firstChild.hash.slice(1));

    }
});

L.control.sidebar = function (sidebar, options) {
    return new L.Control.Sidebar(sidebar, options);
};

/*! noUiSlider - 7.0.10 - 2014-12-27 14:50:47 */

!function(){"use strict";function a(a){return a.split("").reverse().join("")}function b(a,b){return a.substring(0,b.length)===b}function c(a,b){return a.slice(-1*b.length)===b}function d(a,b,c){if((a[b]||a[c])&&a[b]===a[c])throw new Error(b)}function e(a){return"number"==typeof a&&isFinite(a)}function f(a,b){var c=Math.pow(10,b);return(Math.round(a*c)/c).toFixed(b)}function g(b,c,d,g,h,i,j,k,l,m,n,o){var p,q,r,s=o,t="",u="";return i&&(o=i(o)),e(o)?(b!==!1&&0===parseFloat(o.toFixed(b))&&(o=0),0>o&&(p=!0,o=Math.abs(o)),b!==!1&&(o=f(o,b)),o=o.toString(),-1!==o.indexOf(".")?(q=o.split("."),r=q[0],d&&(t=d+q[1])):r=o,c&&(r=a(r).match(/.{1,3}/g),r=a(r.join(a(c)))),p&&k&&(u+=k),g&&(u+=g),p&&l&&(u+=l),u+=r,u+=t,h&&(u+=h),m&&(u=m(u,s)),u):!1}function h(a,d,f,g,h,i,j,k,l,m,n,o){var p,q="";return n&&(o=n(o)),o&&"string"==typeof o?(k&&b(o,k)&&(o=o.replace(k,""),p=!0),g&&b(o,g)&&(o=o.replace(g,"")),l&&b(o,l)&&(o=o.replace(l,""),p=!0),h&&c(o,h)&&(o=o.slice(0,-1*h.length)),d&&(o=o.split(d).join("")),f&&(o=o.replace(f,".")),p&&(q+="-"),q+=o,q=q.replace(/[^0-9\.\-.]/g,""),""===q?!1:(q=Number(q),j&&(q=j(q)),e(q)?q:!1)):!1}function i(a){var b,c,e,f={};for(b=0;b<l.length;b+=1)if(c=l[b],e=a[c],void 0===e)f[c]="negative"!==c||f.negativeBefore?"mark"===c&&"."!==f.thousand?".":!1:"-";else if("decimals"===c){if(!(e>=0&&8>e))throw new Error(c);f[c]=e}else if("encoder"===c||"decoder"===c||"edit"===c||"undo"===c){if("function"!=typeof e)throw new Error(c);f[c]=e}else{if("string"!=typeof e)throw new Error(c);f[c]=e}return d(f,"mark","thousand"),d(f,"prefix","negative"),d(f,"prefix","negativeBefore"),f}function j(a,b,c){var d,e=[];for(d=0;d<l.length;d+=1)e.push(a[l[d]]);return e.push(c),b.apply("",e)}function k(a){return this instanceof k?void("object"==typeof a&&(a=i(a),this.to=function(b){return j(a,g,b)},this.from=function(b){return j(a,h,b)})):new k(a)}var l=["decimals","thousand","mark","prefix","postfix","encoder","decoder","negativeBefore","negative","edit","undo"];window.wNumb=k}(),function(a){"use strict";function b(b){return b instanceof a||a.zepto&&a.zepto.isZ(b)}function c(b,c){return"string"==typeof b&&0===b.indexOf("-inline-")?(this.method=c||"html",this.target=this.el=a(b.replace("-inline-","")||"<div/>"),!0):void 0}function d(b){if("string"==typeof b&&0!==b.indexOf("-")){this.method="val";var c=document.createElement("input");return c.name=b,c.type="hidden",this.target=this.el=a(c),!0}}function e(a){return"function"==typeof a?(this.target=!1,this.method=a,!0):void 0}function f(a,c){return b(a)&&!c?(a.is("input, select, textarea")?(this.method="val",this.target=a.on("change.liblink",this.changeHandler)):(this.target=a,this.method="html"),!0):void 0}function g(a,c){return b(a)&&("function"==typeof c||"string"==typeof c&&a[c])?(this.method=c,this.target=a,!0):void 0}function h(b,c,d){var e=this,f=!1;if(this.changeHandler=function(b){var c=e.formatInstance.from(a(this).val());return c===!1||isNaN(c)?(a(this).val(e.lastSetValue),!1):void e.changeHandlerMethod.call("",b,c)},this.el=!1,this.formatInstance=d,a.each(k,function(a,d){return f=d.call(e,b,c),!f}),!f)throw new RangeError("(Link) Invalid Link.")}function i(a){this.items=[],this.elements=[],this.origin=a}function j(b,c,d,e){0===b&&(b=this.LinkDefaultFlag),this.linkAPI||(this.linkAPI={}),this.linkAPI[b]||(this.linkAPI[b]=new i(this));var f=new h(c,d,e||this.LinkDefaultFormatter);f.target||(f.target=a(this)),f.changeHandlerMethod=this.LinkConfirm(b,f.el),this.linkAPI[b].push(f,f.el),this.LinkUpdate(b)}var k=[c,d,e,f,g];h.prototype.set=function(a){var b=Array.prototype.slice.call(arguments),c=b.slice(1);this.lastSetValue=this.formatInstance.to(a),c.unshift(this.lastSetValue),("function"==typeof this.method?this.method:this.target[this.method]).apply(this.target,c)},i.prototype.push=function(a,b){this.items.push(a),b&&this.elements.push(b)},i.prototype.reconfirm=function(a){var b;for(b=0;b<this.elements.length;b+=1)this.origin.LinkConfirm(a,this.elements[b])},i.prototype.remove=function(){var a;for(a=0;a<this.items.length;a+=1)this.items[a].target.off(".liblink");for(a=0;a<this.elements.length;a+=1)this.elements[a].remove()},i.prototype.change=function(a){if(this.origin.LinkIsEmitting)return!1;this.origin.LinkIsEmitting=!0;var b,c=Array.prototype.slice.call(arguments,1);for(c.unshift(a),b=0;b<this.items.length;b+=1)this.items[b].set.apply(this.items[b],c);this.origin.LinkIsEmitting=!1},a.fn.Link=function(b){var c=this;if(b===!1)return c.each(function(){this.linkAPI&&(a.map(this.linkAPI,function(a){a.remove()}),delete this.linkAPI)});if(void 0===b)b=0;else if("string"!=typeof b)throw new Error("Flag must be string.");return{to:function(a,d,e){return c.each(function(){j.call(this,b,a,d,e)})}}}}(window.jQuery||window.Zepto),function(a){"use strict";function b(b){return a.grep(b,function(c,d){return d===a.inArray(c,b)})}function c(a,b){return Math.round(a/b)*b}function d(a){return"number"==typeof a&&!isNaN(a)&&isFinite(a)}function e(a){var b=Math.pow(10,7);return Number((Math.round(a*b)/b).toFixed(7))}function f(a,b,c){a.addClass(b),setTimeout(function(){a.removeClass(b)},c)}function g(a){return Math.max(Math.min(a,100),0)}function h(b){return a.isArray(b)?b:[b]}function i(a){var b=a.split(".");return b.length>1?b[1].length:0}function j(a,b){return 100/(b-a)}function k(a,b){return 100*b/(a[1]-a[0])}function l(a,b){return k(a,a[0]<0?b+Math.abs(a[0]):b-a[0])}function m(a,b){return b*(a[1]-a[0])/100+a[0]}function n(a,b){for(var c=1;a>=b[c];)c+=1;return c}function o(a,b,c){if(c>=a.slice(-1)[0])return 100;var d,e,f,g,h=n(c,a);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],f+l([d,e],c)/j(f,g)}function p(a,b,c){if(c>=100)return a.slice(-1)[0];var d,e,f,g,h=n(c,b);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],m([d,e],(c-f)*j(f,g))}function q(a,b,d,e){if(100===e)return e;var f,g,h=n(e,a);return d?(f=a[h-1],g=a[h],e-f>(g-f)/2?g:f):b[h-1]?a[h-1]+c(e-a[h-1],b[h-1]):e}function r(a,b,c){var e;if("number"==typeof b&&(b=[b]),"[object Array]"!==Object.prototype.toString.call(b))throw new Error("noUiSlider: 'range' contains invalid value.");if(e="min"===a?0:"max"===a?100:parseFloat(a),!d(e)||!d(b[0]))throw new Error("noUiSlider: 'range' value isn't numeric.");c.xPct.push(e),c.xVal.push(b[0]),e?c.xSteps.push(isNaN(b[1])?!1:b[1]):isNaN(b[1])||(c.xSteps[0]=b[1])}function s(a,b,c){return b?void(c.xSteps[a]=k([c.xVal[a],c.xVal[a+1]],b)/j(c.xPct[a],c.xPct[a+1])):!0}function t(a,b,c,d){this.xPct=[],this.xVal=[],this.xSteps=[d||!1],this.xNumSteps=[!1],this.snap=b,this.direction=c;var e,f=[];for(e in a)a.hasOwnProperty(e)&&f.push([a[e],e]);for(f.sort(function(a,b){return a[0]-b[0]}),e=0;e<f.length;e++)r(f[e][1],f[e][0],this);for(this.xNumSteps=this.xSteps.slice(0),e=0;e<this.xNumSteps.length;e++)s(e,this.xNumSteps[e],this)}function u(a,b){if(!d(b))throw new Error("noUiSlider: 'step' is not numeric.");a.singleStep=b}function v(b,c){if("object"!=typeof c||a.isArray(c))throw new Error("noUiSlider: 'range' is not an object.");if(void 0===c.min||void 0===c.max)throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");b.spectrum=new t(c,b.snap,b.dir,b.singleStep)}function w(b,c){if(c=h(c),!a.isArray(c)||!c.length||c.length>2)throw new Error("noUiSlider: 'start' option is incorrect.");b.handles=c.length,b.start=c}function x(a,b){if(a.snap=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'snap' option must be a boolean.")}function y(a,b){if(a.animate=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'animate' option must be a boolean.")}function z(a,b){if("lower"===b&&1===a.handles)a.connect=1;else if("upper"===b&&1===a.handles)a.connect=2;else if(b===!0&&2===a.handles)a.connect=3;else{if(b!==!1)throw new Error("noUiSlider: 'connect' option doesn't match handle count.");a.connect=0}}function A(a,b){switch(b){case"horizontal":a.ort=0;break;case"vertical":a.ort=1;break;default:throw new Error("noUiSlider: 'orientation' option is invalid.")}}function B(a,b){if(!d(b))throw new Error("noUiSlider: 'margin' option must be numeric.");if(a.margin=a.spectrum.getMargin(b),!a.margin)throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")}function C(a,b){if(!d(b))throw new Error("noUiSlider: 'limit' option must be numeric.");if(a.limit=a.spectrum.getMargin(b),!a.limit)throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")}function D(a,b){switch(b){case"ltr":a.dir=0;break;case"rtl":a.dir=1,a.connect=[0,2,1,3][a.connect];break;default:throw new Error("noUiSlider: 'direction' option was not recognized.")}}function E(a,b){if("string"!=typeof b)throw new Error("noUiSlider: 'behaviour' must be a string containing options.");var c=b.indexOf("tap")>=0,d=b.indexOf("drag")>=0,e=b.indexOf("fixed")>=0,f=b.indexOf("snap")>=0;a.events={tap:c||f,drag:d,fixed:e,snap:f}}function F(a,b){if(a.format=b,"function"==typeof b.to&&"function"==typeof b.from)return!0;throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")}function G(b){var c,d={margin:0,limit:0,animate:!0,format:Z};return c={step:{r:!1,t:u},start:{r:!0,t:w},connect:{r:!0,t:z},direction:{r:!0,t:D},snap:{r:!1,t:x},animate:{r:!1,t:y},range:{r:!0,t:v},orientation:{r:!1,t:A},margin:{r:!1,t:B},limit:{r:!1,t:C},behaviour:{r:!0,t:E},format:{r:!1,t:F}},b=a.extend({connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal"},b),a.each(c,function(a,c){if(void 0===b[a]){if(c.r)throw new Error("noUiSlider: '"+a+"' is required.");return!0}c.t(d,b[a])}),d.style=d.ort?"top":"left",d}function H(a,b,c){var d=a+b[0],e=a+b[1];return c?(0>d&&(e+=Math.abs(d)),e>100&&(d-=e-100),[g(d),g(e)]):[d,e]}function I(a){a.preventDefault();var b,c,d=0===a.type.indexOf("touch"),e=0===a.type.indexOf("mouse"),f=0===a.type.indexOf("pointer"),g=a;return 0===a.type.indexOf("MSPointer")&&(f=!0),a.originalEvent&&(a=a.originalEvent),d&&(b=a.changedTouches[0].pageX,c=a.changedTouches[0].pageY),(e||f)&&(f||void 0!==window.pageXOffset||(window.pageXOffset=document.documentElement.scrollLeft,window.pageYOffset=document.documentElement.scrollTop),b=a.clientX+window.pageXOffset,c=a.clientY+window.pageYOffset),g.points=[b,c],g.cursor=e,g}function J(b,c){var d=a("<div><div/></div>").addClass(Y[2]),e=["-lower","-upper"];return b&&e.reverse(),d.children().addClass(Y[3]+" "+Y[3]+e[c]),d}function K(a,b,c){switch(a){case 1:b.addClass(Y[7]),c[0].addClass(Y[6]);break;case 3:c[1].addClass(Y[6]);case 2:c[0].addClass(Y[7]);case 0:b.addClass(Y[6])}}function L(a,b,c){var d,e=[];for(d=0;a>d;d+=1)e.push(J(b,d).appendTo(c));return e}function M(b,c,d){return d.addClass([Y[0],Y[8+b],Y[4+c]].join(" ")),a("<div/>").appendTo(d).addClass(Y[1])}function N(b,c,d){function e(){return C[["width","height"][c.ort]]()}function j(a){var b,c=[E.val()];for(b=0;b<a.length;b+=1)E.trigger(a[b],c)}function k(a){return 1===a.length?a[0]:c.dir?a.reverse():a}function l(a){return function(b,c){E.val([a?null:c,a?c:null],!0)}}function m(b){var c=a.inArray(b,N);E[0].linkAPI&&E[0].linkAPI[b]&&E[0].linkAPI[b].change(J[c],D[c].children(),E)}function n(b,d){var e=a.inArray(b,N);return d&&d.appendTo(D[e].children()),c.dir&&c.handles>1&&(e=1===e?0:1),l(e)}function o(){var a,b;for(a=0;a<N.length;a+=1)this.linkAPI&&this.linkAPI[b=N[a]]&&this.linkAPI[b].reconfirm(b)}function p(a,b,d,e){return a=a.replace(/\s/g,W+" ")+W,b.on(a,function(a){return E.attr("disabled")?!1:E.hasClass(Y[14])?!1:(a=I(a),a.calcPoint=a.points[c.ort],void d(a,e))})}function q(a,b){var c,d=b.handles||D,f=!1,g=100*(a.calcPoint-b.start)/e(),h=d[0][0]!==D[0][0]?1:0;c=H(g,b.positions,d.length>1),f=v(d[0],c[h],1===d.length),d.length>1&&(f=v(d[1],c[h?0:1],!1)||f),f&&j(["slide"])}function r(b){a("."+Y[15]).removeClass(Y[15]),b.cursor&&a("body").css("cursor","").off(W),U.off(W),E.removeClass(Y[12]),j(["set","change"])}function s(b,c){1===c.handles.length&&c.handles[0].children().addClass(Y[15]),b.stopPropagation(),p(X.move,U,q,{start:b.calcPoint,handles:c.handles,positions:[F[0],F[D.length-1]]}),p(X.end,U,r,null),b.cursor&&(a("body").css("cursor",a(b.target).css("cursor")),D.length>1&&E.addClass(Y[12]),a("body").on("selectstart"+W,!1))}function t(b){var d,g=b.calcPoint,h=0;b.stopPropagation(),a.each(D,function(){h+=this.offset()[c.style]}),h=h/2>g||1===D.length?0:1,g-=C.offset()[c.style],d=100*g/e(),c.events.snap||f(E,Y[14],300),v(D[h],d),j(["slide","set","change"]),c.events.snap&&s(b,{handles:[D[h]]})}function u(a){var b,c;if(!a.fixed)for(b=0;b<D.length;b+=1)p(X.start,D[b].children(),s,{handles:[D[b]]});a.tap&&p(X.start,C,t,{handles:D}),a.drag&&(c=C.find("."+Y[7]).addClass(Y[10]),a.fixed&&(c=c.add(C.children().not(c).children())),p(X.start,c,s,{handles:D}))}function v(a,b,d){var e=a[0]!==D[0][0]?1:0,f=F[0]+c.margin,h=F[1]-c.margin,i=F[0]+c.limit,j=F[1]-c.limit;return D.length>1&&(b=e?Math.max(b,f):Math.min(b,h)),d!==!1&&c.limit&&D.length>1&&(b=e?Math.min(b,i):Math.max(b,j)),b=G.getStep(b),b=g(parseFloat(b.toFixed(7))),b===F[e]?!1:(a.css(c.style,b+"%"),a.is(":first-child")&&a.toggleClass(Y[17],b>50),F[e]=b,J[e]=G.fromStepping(b),m(N[e]),!0)}function w(a,b){var d,e,f;for(c.limit&&(a+=1),d=0;a>d;d+=1)e=d%2,f=b[e],null!==f&&f!==!1&&("number"==typeof f&&(f=String(f)),f=c.format.from(f),(f===!1||isNaN(f)||v(D[e],G.toStepping(f),d===3-c.dir)===!1)&&m(N[e]))}function x(a){if(E[0].LinkIsEmitting)return this;var b,d=h(a);return c.dir&&c.handles>1&&d.reverse(),c.animate&&-1!==F[0]&&f(E,Y[14],300),b=D.length>1?3:1,1===d.length&&(b=1),w(b,d),j(["set"]),this}function y(){var a,b=[];for(a=0;a<c.handles;a+=1)b[a]=c.format.to(J[a]);return k(b)}function z(){return a(this).off(W).removeClass(Y.join(" ")).empty(),delete this.LinkUpdate,delete this.LinkConfirm,delete this.LinkDefaultFormatter,delete this.LinkDefaultFlag,delete this.reappend,delete this.vGet,delete this.vSet,delete this.getCurrentStep,delete this.getInfo,delete this.destroy,d}function A(){var b=a.map(F,function(a,b){var c=G.getApplicableStep(a),d=i(String(c[2])),e=J[b],f=100===a?null:c[2],g=Number((e-c[2]).toFixed(d)),h=0===a?null:g>=c[1]?c[2]:c[0]||!1;return[[h,f]]});return k(b)}function B(){return d}var C,D,E=a(b),F=[-1,-1],G=c.spectrum,J=[],N=["lower","upper"].slice(0,c.handles);if(c.dir&&N.reverse(),b.LinkUpdate=m,b.LinkConfirm=n,b.LinkDefaultFormatter=c.format,b.LinkDefaultFlag="lower",b.reappend=o,E.hasClass(Y[0]))throw new Error("Slider was already initialized.");C=M(c.dir,c.ort,E),D=L(c.handles,c.dir,C),K(c.connect,E,D),u(c.events),b.vSet=x,b.vGet=y,b.destroy=z,b.getCurrentStep=A,b.getOriginalOptions=B,b.getInfo=function(){return[G,c.style,c.ort]},E.val(c.start)}function O(a){var b=G(a,this);return this.each(function(){N(this,b,a)})}function P(b){return this.each(function(){if(!this.destroy)return void a(this).noUiSlider(b);var c=a(this).val(),d=this.destroy(),e=a.extend({},d,b);a(this).noUiSlider(e),this.reappend(),d.start===e.start&&a(this).val(c)})}function Q(){return this[0][arguments.length?"vSet":"vGet"].apply(this[0],arguments)}function R(b,c,d,e){if("range"===c||"steps"===c)return b.xVal;if("count"===c){var f,g=100/(d-1),h=0;for(d=[];(f=h++*g)<=100;)d.push(f);c="positions"}return"positions"===c?a.map(d,function(a){return b.fromStepping(e?b.getStep(a):a)}):"values"===c?e?a.map(d,function(a){return b.fromStepping(b.getStep(b.toStepping(a)))}):d:void 0}function S(c,d,e,f){var g=c.direction,h={},i=c.xVal[0],j=c.xVal[c.xVal.length-1],k=!1,l=!1,m=0;return c.direction=0,f=b(f.slice().sort(function(a,b){return a-b})),f[0]!==i&&(f.unshift(i),k=!0),f[f.length-1]!==j&&(f.push(j),l=!0),a.each(f,function(b){var g,i,j,n,o,p,q,r,s,t,u=f[b],v=f[b+1];if("steps"===e&&(g=c.xNumSteps[b]),g||(g=v-u),u!==!1&&void 0!==v)for(i=u;v>=i;i+=g){for(n=c.toStepping(i),o=n-m,r=o/d,s=Math.round(r),t=o/s,j=1;s>=j;j+=1)p=m+j*t,h[p.toFixed(5)]=["x",0];q=a.inArray(i,f)>-1?1:"steps"===e?2:0,!b&&k&&(q=0),i===v&&l||(h[n.toFixed(5)]=[i,q]),m=n}}),c.direction=g,h}function T(b,c,d,e,f,g){function h(a){return["-normal","-large","-sub"][a]}function i(a,c,d){return'class="'+c+" "+c+"-"+k+" "+c+h(d[1],d[0])+'" style="'+b+": "+a+'%"'}function j(a,b){d&&(a=100-a),b[1]=b[1]&&f?f(b[0],b[1]):b[1],l.append("<div "+i(a,"noUi-marker",b)+"></div>"),b[1]&&l.append("<div "+i(a,"noUi-value",b)+">"+g.to(b[0])+"</div>")}var k=["horizontal","vertical"][c],l=a("<div/>");return l.addClass("noUi-pips noUi-pips-"+k),a.each(e,j),l}var U=a(document),V=a.fn.val,W=".nui",X=window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"},Y=["noUi-target","noUi-base","noUi-origin","noUi-handle","noUi-horizontal","noUi-vertical","noUi-background","noUi-connect","noUi-ltr","noUi-rtl","noUi-dragable","","noUi-state-drag","","noUi-state-tap","noUi-active","","noUi-stacking"];t.prototype.getMargin=function(a){return 2===this.xPct.length?k(this.xVal,a):!1},t.prototype.toStepping=function(a){return a=o(this.xVal,this.xPct,a),this.direction&&(a=100-a),a},t.prototype.fromStepping=function(a){return this.direction&&(a=100-a),e(p(this.xVal,this.xPct,a))},t.prototype.getStep=function(a){return this.direction&&(a=100-a),a=q(this.xPct,this.xSteps,this.snap,a),this.direction&&(a=100-a),a},t.prototype.getApplicableStep=function(a){var b=n(a,this.xPct),c=100===a?2:1;return[this.xNumSteps[b-2],this.xVal[b-c],this.xNumSteps[b-c]]},t.prototype.convert=function(a){return this.getStep(this.toStepping(a))};var Z={to:function(a){return a.toFixed(2)},from:Number};a.fn.val=function(b){function c(a){return a.hasClass(Y[0])?Q:V}if(!arguments.length){var d=a(this[0]);return c(d).call(d)}var e=a.isFunction(b);return this.each(function(d){var f=b,g=a(this);e&&(f=b.call(this,d,g.val())),c(g).call(g,f)})},a.fn.noUiSlider=function(a,b){switch(a){case"step":return this[0].getCurrentStep();case"options":return this[0].getOriginalOptions()}return(b?P:O).call(this,a)},a.fn.noUiSlider_pips=function(b){var c=b.mode,d=b.density||1,e=b.filter||!1,f=b.values||!1,g=b.format||{to:Math.round},h=b.stepped||!1;return this.each(function(){var b=this.getInfo(),i=R(b[0],c,f,h),j=S(b[0],d,c,i);return a(this).append(T(b[1],b[2],b[0].direction,j,e,g))})}}(window.jQuery||window.Zepto);
'use strict';



angular.module('denguemapsApp')
  .factory('mapServices', ["$http", "$q", function($http, $q) {
  	var dengueURL = window.basePath + '/denguemaps-server/rest';
    var geoURL = window.basePath + '/geoserver/sf/ows';

    return {
  		getRiesgosDepartamentos: getRiesgosDepartamentos,
      getRiesgosDistritos: getRiesgosDistritos,
      getRiesgosAsuncion: getRiesgosAsuncion,
  		getDepartamentos: getDepartamentos,
      getDistritos: getDistritos,
      getBarrios: getBarrios,
      getCasosDepartamentos: getCasosDepartamentos,
      getIncidenciaDepartamentos: getIncidenciaDepartamentos,
      getIncidenciaDistritos: getIncidenciaDistritos,
      getIncidenciaAsuncion: getIncidenciaAsuncion,
      getPoblacionPorDepto: getPoblacionPorDepto,
      getPoblacionPorDistrito: getPoblacionPorDistrito,
      getPoblacionPorBarrioAsu: getPoblacionPorBarrioAsu
  	};
  
  	function getRiesgosDepartamentos(anio){
  		var defered = $q.defer();
  		var promise = defered.promise;
      var url = dengueURL + '/notificacion/riesgo/' + anio;
  		$http.get(url, {cache: true})
  			.success(function(data){
  				defered.resolve(data);
  			})
  			.error(function(err){
  				defered.reject(err);
  			});
  		return promise;
  	}

    function getRiesgosDistritos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/riesgo/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getRiesgosAsuncion(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/riesgo/asuncion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    /* EN LA OFICINA AGREGARLE EL NRO 2 */
  	function getDepartamentos(){
  		var defered = $q.defer();
  		var promise = defered.promise;
		  //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      //var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay_simply&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay_simply&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
  		$http.jsonp(url, {cache: true})
  			.success(function(data){
  				defered.resolve(data);
  			})
  			.error(function(err){
  				defered.reject(err);
  			});
  		return promise;
  	}

    /* ESTOS DOS TDV NO SE ESTAN USANDO, FALTA PROBAR */
    function getDistritos(dpto){
      var defered = $q.defer();
      var promise = defered.promise;
      //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=first_dp_1=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      //var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=dpto=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getBarrios(dpto){
      var defered = $q.defer();
      var promise = defered.promise;
      //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      //var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getCasosDepartamentos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/filtrosmapa?anio=' + anio +'&confirmado='+1+'&descartado='+1+'&sospechoso='+1+'&f=0&m=0';
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getIncidenciaDepartamentos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/incidencia/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getIncidenciaDistritos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/incidencia/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getIncidenciaAsuncion(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/incidencia/asuncion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }


    function getPoblacionPorDepto(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/poblacion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getPoblacionPorDistrito(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/poblacion/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getPoblacionPorBarrioAsu(){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/poblacion/asuncion/';
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }
}]);
'use strict';



angular.module('denguemapsApp')
  .factory('mapUtil', ["mapServices", "$q", function(mapServices, $q) {
  		return {
  			getInfo: getInfo,
  		};

  		function getInfo(){
			info = L.control();
		    info.onAdd = function (map) {
		        this._div = L.DomUtil.create('div', 'info');
		        this.update();
		        return this._div;
		    };
		    info.update = function (props) {

		        if(props){

		            var dep = props.dpto_desc;
		            //if(SMV.onriesgo){
		                var mapSem = riesgoSemanaDep;
		            /*}else{
		                var mapSem = SMV.mapSemFil;
		            }*/

		            var nroNS = '0';
		            try{
		                nroNS = mapSem[dep]["cantidad"];
		            }catch(e){

		            }
		          this._div.innerHTML =  '<h2>Año: '+anio+' - Semana: '+semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
		        }
		    };
		    info.updateDrillDown = function (props){

		        if(props){
		        	console.log(props);
		            var dep = props.dpto_desc;
		            var depAsu = props.dpto_desc;
		            var dis = props.dist_desc;
		            var mapSem = riesgoSemanaDis;
		            var nroNS = '0';

		            var info;
		            if(depAsu == 'ASUNCION'){
		            	dis = props.dist_desc;
		                var key = depAsu+'-'+props.barlo_desc;
		                try{
		                    nroNS = mapSem[key]["cantidad"];
		                }catch(e){

		                }
		                info = '<h2>Año: '+anio+' - Semana: '+semana+'<\/h2><h2>Dpto: '+depAsu+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Barrio: '+props.barlo_desc+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
		            } else {
		                var key = dep+'-'+dis;
		                try{
		                    nroNS = mapSem[key]["cantidad"];
		                }catch(e){

		                }
		                info = '<h2>Año: '+anio+' - Semana: '+semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
		            }
		          this._div.innerHTML =  info;
		        }
	    	};
	    	return info;
	    }

}]);
'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:fileModel
 * @description
 * # fileModel
 */

angular.module('denguemapsApp')
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
}]);
'use strict';



angular.module('denguemapsApp')
  .factory('fileUpload', ['$http', '$q', function ($http, $q, toastr) {
    return {
      uploadFileToUrl: uploadFileToUrl
    }

    function uploadFileToUrl(file, uploadUrl){
      var defered = $q.defer();
      var promise = defered.promise;
      var fd = new FormData();
      fd.append('file', file);
      $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
          //console.log(data);
          defered.resolve(data);
        })
        .error(function(err){
          //toastr.error('La estructura de columnas del archivo no es válida','Error');
          defered.reject(err);
        });
      return promise;
    }

}]);

'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaRiesgos', ["$rootScope", "$http", "mapServices", "$q", function ($rootScope,  $http, mapServices, $q) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			detalle:'='
		},
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		//mapCtrl.imprimir();
			mapCtrl.imprimir("mapaRiesgos");
			mapCtrl.cleanMap();
			
			var departamentos;
			var map = mapCtrl.getMap();
			var STATE = $rootScope.RIESGO_STATE;
			console.log("STATE::");
			console.log($rootScope.RIESGO_STATE);
			if(STATE === undefined){
				console.log("undefined::");
				STATE = {};
				STATE.inZoom = false;
				departamentos = mapCtrl.getDepartamentos();
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature: onEachFeature}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					return init($("#sMonth").val(), $("#sYear").val(), STATE.geoJsonLayer);
				}).then(function(data){
	          		getRiesgosLegend().addTo(map);
					getRiesgoInfo().addTo(map);
	          		data.setStyle(getStyle);
	          		$rootScope.RIESGO_STATE = STATE;
	          	});
	          	
			} else {
				STATE.geoJsonLayer.addTo(map);
				if(STATE.inZoom){
					STATE.drillDownLayer.addTo(map);
					map.fitBounds(STATE.drillDownLayer.getBounds());
					STATE.backButton.addTo(map);
					mapCtrl.addControl(STATE.backButton);
					mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
				}
				STATE.info.addTo(map);
				STATE.riesgosLegend.addTo(map);
				mapCtrl.addControl(STATE.info);
				mapCtrl.addControl(STATE.riesgosLegend);
				mapCtrl.setActualLayer(STATE.geoJsonLayer);
			}

			/* Control de opacidad */
			//Create the opacity controls
	        /*var opacitySlider = new L.Control.opacitySlider();
	        map.addControl(opacitySlider);

	    	//Specify the layer for which you want to modify the opacity. Note that the setOpacityLayer() method applies to all the controls.
	    	//You only need to call it once.
	        opacitySlider.setOpacityLayer(departamentos);

	    	//Set initial opacity to 0.5 (Optional)
        	departamentos.setOpacity(0.5);*/
        	//mapUtil.setMap(map);

        	$("#sMonth").on({
				slide: function(){
					reloadSem($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});

        	
			var backClass =  L.Control.extend({
		        options: {
		            position: 'topright'
		        },

		        onAdd: function (map) {
		            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
		            L.DomEvent
		                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
		                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
		            .addListener(controlDiv, 'click', drillUp);

		            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-interior', controlDiv);
		            controlUI.title = 'Map Commands';
		            return controlDiv;
		        }
		    });
		    

			function init(sem, ano, layer){
	  			var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			return mapServices.getRiesgosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosRiesgo =  data;
	    				return mapServices.getRiesgosDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					console.log("riesgos de distritos- ");
	  					STATE.distritosRiesgo = data;
	  					return mapServices.getRiesgosAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					console.log("riesgos de asuncion- ");
	  					STATE.asuncionRiesgo = data;
	  					reloadSem(STATE.semana);
	  					defered.resolve(layer);
	  					return promise;
	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	  		}

	  		function reloadAnio(sem, ano){
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			mapServices.getRiesgosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosRiesgo =  data;
	    				return mapServices.getRiesgosDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					console.log("riesgos de distritos- ");
	  					STATE.distritosRiesgo = data;
	  					return mapServices.getRiesgosAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					console.log("riesgos de asuncion- ");
	  					STATE.asuncionRiesgo = data;
	  					reloadSem(STATE.semana);

	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	  		}

			function reloadSem(semana){
				STATE.semana = semana;
			    var rasu;
			    var rcen;
			    var mapSem = new Object();
			    var mapSemDis = new Object();

			    //Cargar el json de los riesgos por semana
			    //riesgos de todas las semanas de los departamentos
			    var riesgo = STATE.departamentosRiesgo;
			    //riesgos de todas las semana de los distritos
			    var riesgoDis = STATE.distritosRiesgo;
			    //riesgos de todas las semanas de los barrios de asuncion
			    var riesgoAsu = STATE.asuncionRiesgo;
			   	console.log("reloadSem - - -");

			    for(var i=0; i<riesgo.length; i++){
			        var obj = riesgo[i];
			        if(obj["semana"]== semana ){
			            mapSem[obj["departamento"]]= obj;
			            if(obj["departamento"]=="CENTRAL"){
			                rcen = obj;
			            }
			            if(obj["departamento"]=="ASUNCION"){
			                rasu = obj;
			            }
			        }
			    }
			    if(rasu){
			       try{
			            rasu["riesgo"] = rcen["riesgo"];
			        }catch(e){
			            rasu["riesgo"] = 'RB';
			        }
			        mapSem["ASUNCION"] = rasu;
			    }
			    //Riesgo de los departamentos de la semana seleccionada
			    STATE.riesgoSemanaDep = mapSem;

			    for(var i=0; i<riesgoDis.length; i++){
			        var obj = riesgoDis[i];
			        if(obj["semana"]== semana ){

			            mapSemDis[obj["distrito"]]= obj;
			            /*if(obj["departamento"]=="CENTRAL"){
			                rcen = obj;
			            }*/
			            if(obj["distrito"] == "ASUNCION"){
			                //rasu = obj;
			            }
			        }
			    }
			    //Incluir en el mapSemDis los barrios de Asuncion
			    for(var i=0; i<riesgoAsu.length; i++){
			        var obj = riesgoAsu[i];
			        if(obj["semana"] == semana){
			            mapSemDis[obj["barrio"]] = obj;
			        }

			    }
			   	//Riesgos de los distritos de la semana seleccionada
			    STATE.riesgoSemanaDis = mapSemDis;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    if(STATE.inZoom){
			    	STATE.drillDownLayer.setStyle(getStyleDrillDown);
			    }
			}


	  		function getRiesgosLegend(){
	  			STATE.riesgosLegend = L.control({
			        position: 'bottomright'
			    });
			    STATE.riesgosLegend.onAdd = function(map) {
			        var div = L.DomUtil.create('div', 'info legend'), labels = [];
			        labels.push('<i style="background:' + getColor('E') + '"></i> ' + 'Epidemia');
			        labels.push('<i style="background:' + getColor('RA') + '"></i> ' + 'Riesgo alto');
			        labels.push('<i style="background:' + getColor('RM') + '"></i> ' + 'Riesgo medio');
			        labels.push('<i style="background:' + getColor('RB') + '"></i> ' + 'Riesgo bajo');
			        div.innerHTML = '<span>Umbrales de riesgo</span><br>' + labels.join('<br>')
			        return div;
			    };
			    mapCtrl.addControl(STATE.riesgosLegend);
			    return STATE.riesgosLegend;
	  		}

	  		function getRiesgoInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {
			    	
			        if(props){

			            var dep = props.dpto_desc;
			            //if(SMV.onriesgo){
			                var mapSem = STATE.riesgoSemanaDep;
			            /*}else{
			                var mapSem = SMV.mapSemFil;
			            }*/

			            var nroNS = '0';
			            try{
			                nroNS = mapSem[dep]["cantidad"];
			            }catch(e){

			            }
			          this._div.innerHTML =  '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			        }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){

			            var dep = props.dpto_desc;
			            var depAsu = props.dpto_desc;
			            var dis = props.dist_desc;
			            var mapSem = STATE.riesgoSemanaDis;
			            var nroNS = '0';

			            var info;
			            if(depAsu == 'ASUNCION'){
			            	dis = props.dist_desc;
			                var key = depAsu+'-'+props.barlo_desc;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+depAsu+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Barrio: '+props.barlo_desc+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			            } else {
			                var key = dep+'-'+dis;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			            }
			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }

			function onEachFeature(feature, layer) {
			    layer.on({
			        mouseover: mouseover,
			        mouseout: mouseout,
			        click: zoomToFeature
			    });

			}

			function mouseover(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.update(layer.feature.properties);
			}

			/*Evento al salir el puntero de un departamento*/
			function mouseout(e) {
			    //geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();

			}

			/*Zoom al hacer click en un departamento*/
			function zoomToFeature(e) {
				
			    var target = e.target;
			    //pedir distritos?
			    console.log(target.feature.properties.dpto_desc);
			    if(!STATE.inZoom){
				   	STATE.backButton = new backClass();
			        map.addControl(STATE.backButton);
			        mapCtrl.addControl(STATE.backButton);
			    }
			    if(target.feature.properties.dpto_desc == 'ASUNCION'){
			    	 mapServices.getBarrios(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    console.log(data);
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
	    			});
			    } else {
			    	mapServices.getDistritos(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    console.log(data);
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					    console.log(STATE.drillDownLayer);
	    			});
			    }
			}

	    	function getColor(d) {
			    return d == 'E' ? '#FE0516' :
			        d == 'RA' ? '#FF6905' :
			        d == 'RM' ? '#FFB905' :
			        '#FFF96D';
			}

			/*Estilo de la capa de departamento de acuedo a los valores de riesgo*/
			function getStyle(feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.riesgoSemanaDep;
			    var color = 'NONE';
			    try{
			        color = mapSem[n]["riesgo"];
			    }catch(e){
			    }
			    if(STATE.inZoom){
			    	return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0,
			            fillColor: getColor(color)
			        };
			    }
			  	else{
			  		return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			           	fillOpacity: 0.5,
			            fillColor: getColor(color)
			        };
			  	}

			}

			/*Estilo de la capa de distritos de acuedo a los valores de riesgo*/
			function getStyleDrillDown(feature) {
			    var prop = feature.properties;
			    var n = prop.dpto_desc+'-'+prop.dist_desc;
			    var mapSem = STATE.riesgoSemanaDis;
			    var color = 'NONE';
			    if(prop.dpto_desc == 'ASUNCION'){
			        n = prop.dpto_desc+'-'+prop.barlo_desc;
			    }
			   try{
			        color = mapSem[n]["riesgo"];
			       
			    }catch(e){
			    }
			   
			    return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0.5, 
			            fillColor: getColor(color) 
			        };
			}
			/*Eventos para cada distrito*/
			function onEachFeatureDrillDown(feature, layer) {
			    layer.on({
			        mouseover: mouseoverDrillDown,
			        mouseout: mouseoutDrillDown,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverDrillDown(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''
			        
			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.updateDrillDown(layer.feature.properties);
			}
			/*Evento al salir el puntero de un distrito*/
			function mouseoutDrillDown(e) {
			    STATE.drillDownLayer.resetStyle(e.target);
			    STATE.info.updateDrillDown();
			   
			}
			function drillUp(){
			    //map.fitBounds(SMV.STATE.geoJsonLayer.getBounds());
			    map.removeLayer(STATE.drillDownLayer);
			    STATE.drillDownLayer = undefined;
			   	STATE.inZoom = false;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    STATE.backButton.removeFrom(map);
			    mapCtrl.removeControl(STATE.backButton);
			    map.setView([-23.388, -60.189], 7);
			}

	    }
	};
}]);
'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaReportes', ["$rootScope", "$http", "mapServices", "$q", function ($rootScope,  $http, mapServices, $q) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			detalle:'='
		},
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		//mapCtrl.imprimir();
			mapCtrl.imprimir("mapaReportes");
			mapCtrl.cleanMap();
			var STATE = $rootScope.CASOS_STATE;
			var map = mapCtrl.getMap();
			var departamentos = mapCtrl.getDepartamentos();
			if(STATE === undefined){
				STATE = {};
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature:onEachFeatureNotificaciones}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					return init($("#sMonth").val(), $("#sYear").val(), STATE.geoJsonLayer);
				}).then(function(data){
					getLegend().addTo(map);
					getInfo().addTo(map);
					data.setStyle(getStyleNotificaciones);
	          		$rootScope.CASOS_STATE = STATE;
	          	});
				
			} else {
				STATE.geoJsonLayer.addTo(map);
				STATE.legendNoti.addTo(map);
				STATE.info.addTo(map);
				mapCtrl.setActualLayer(STATE.geoJsonLayer);
				mapCtrl.addControl(STATE.legendNoti);
				mapCtrl.addControl(STATE.info);
			}
			$("#sMonth").on({
				change: function(){
					reloadNotificaciones($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});
			
			function init(sem, ano, layer){
				var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			return mapServices.getCasosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.resFiltro = data;
	    				console.log(data);
	  					reloadNotificaciones(STATE.semana);
	  					defered.resolve(layer);
	  					return promise;
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			});
			}

			function reloadAnio(sem, ano){
				var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			mapServices.getCasosDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.resFiltro = data;
	    				console.log(data);
	  					reloadNotificaciones(STATE.semana);
	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			});
			}

			function reloadNotificaciones (semana) {
				STATE.semana = semana;
			    var mapSemFil = new Object();
		        for(var i=0; i<STATE.resFiltro.length; i++){
		            var obj = STATE.resFiltro[i];

		            if(obj["semana"] == semana){
		                mapSemFil[obj["departamento"]] = obj;
		            }
		        }
		        STATE.casosSemana = mapSemFil;
		        STATE.geoJsonLayer.setStyle(getStyleNotificaciones);
			}

			function getColorNotificaciones(d) {
			    return    d > 1000    ? '#FE0516' :
			            d > 500    ? '#FD6C24' :
			            d > 100    ? '#FFC56E' :
			            d > 50    ? '#FDFC58' :
			            d > 20    ? '#DBFB69' :
			            d > 10    ? '#BCFD82' :
			            d > 1    ? '#8EF435' :
			                        '#FFFFFF';
            };
           
            function getStyleNotificaciones (feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.casosSemana;    
			    var color = 'NONE';

			    try{
			        color = mapSem[n]["cantidad"];
			    }catch(e){
			    }
			 
			    return { weight: 2,
			        opacity: 1,
			        color: 'white',
			        dashArray: '3',
			        fillOpacity: 0.8, 
			        fillColor: getColorNotificaciones(color) 
			    };
			}
    		function onEachFeatureNotificaciones(feature, layer) {
			    layer.on({
			        mouseover: mouseoverNotificaciones,
			        mouseout: mouseoutNotificaciones,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverNotificaciones(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''
			        
			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			   STATE.info.update(layer.feature.properties);
			}

			/*Evento al salir el puntero de un distrito*/
			function mouseoutNotificaciones(e) {
			   // STATE.geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();
			}
			function getLegend(){
				STATE.legendNoti = L.control({position: 'bottomright'});
			    STATE.legendNoti.onAdd = function (map) {
			        var div = L.DomUtil.create('div', 'info legend'),
			            grades = [1, 10, 20, 50, 100, 500, 1000/*, 200, 300, 400, 500, 600, 700*/],
			            labels = [],
			            from, to;
			        labels.push('<i style="background:' + getColorNotificaciones(0) + '"></i> ' + '0');
			        for (var i = 0; i < grades.length; i++) {
			            from = grades[i];
			            to = grades[i + 1];
			            labels.push(
			                '<i style="background:' + getColorNotificaciones(from + 1) + '"></i> ' +
			                from + (to ? '&ndash;' + to : '+'));
			        }
			        div.innerHTML = '<span>N&uacutemero de Notificaciones</span><br>' + labels.join('<br>')
			        return div;
			    };
			    mapCtrl.addControl(STATE.legendNoti);
			    return STATE.legendNoti;
			}

			function getInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {

			        if(props){

			            var dep = props.dpto_desc;			            
		                var mapSem = STATE.casosSemana;
			            var nroNS = '0';
			            try{
			                nroNS = mapSem[dep]["cantidad"];
			            }catch(e){

			            }
			          this._div.innerHTML =  '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			        }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){

			            var dep = props.first_dp_1;
			            var depAsu = props.dpto_desc;
			            var dis = props.first_di_1;
			            var mapSem = STATE.riesgoSemanaDis;
			            var nroNS = '0';

			            var info;
			            if(depAsu == 'ASUNCION'){
			            	dis = props.dist_desc;
			                var key = depAsu+'-'+props.barlo_desc;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+depAsu+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Barrio: '+props.barlo_desc+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			            } else {
			                var key = dep+'-'+dis;
			                try{
			                    nroNS = mapSem[key]["cantidad"];
			                }catch(e){

			                }
			                info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Notificaciones: '+nroNS+'<\/h2>';
			            }
			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }
        }
    }
}]);
'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaIncidencias', ["$rootScope", "$http", "mapServices", "$q", "usSpinnerService", function ($rootScope,  $http, mapServices, $q, usSpinnerService) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			data:'='
		},
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		 var finishedLoading = function() {
	            usSpinnerService.stop('spinner-dropdown');
	        };

	        var startLoading = function() {
	          usSpinnerService.spin('spinner-dropdown');
	        };
    		
			mapCtrl.cleanMap();
			var clasificaciones = {
				"incidencia":
				{
					casos: "casos",
					legend: "confirmados + sospechosos"
				},
				"incidencia_sospechosos":
				{
					casos: "casos_sospechosos",
					legend: "sospechosos"
				},
				"incidencia_confirmados":
				{
					casos: "casos_confirmados",
					legend: "confirmados"
				}
			};
			startLoading();
			var departamentos;
			var map = mapCtrl.getMap();
			var STATE = $rootScope.INCIDENCIA_STATE;
			console.log("STATE::");
			console.log($rootScope.INCIDENCIA_STATE);
			if(STATE === undefined){
				console.log("undefined::");
				STATE = {};
				STATE.inZoom = false;
				STATE.clasificacion = "incidencia";
				departamentos = mapCtrl.getDepartamentos();
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature: onEachFeature}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					/*var opacitySlider = new L.Control.opacitySlider();
	        		map.addControl(opacitySlider);
	        		console.log('opacity',opacitySlider);
	        		//Specify the layer for which you want to modify the opacity. Note that the setOpacityLayer() method applies to all the controls.
	    			//You only need to call it once.
	        		opacitySlider.setOpacityLayer(STATE.geoJsonLayer);
	        		STATE.geoJsonLayer.setOpacity(0.7);*/
					return init($("#sMonth").val(), $("#sYear").val(), STATE.geoJsonLayer);
				}).then(function(data){
	          		getIncidenciasLegend().addTo(map);
					getIncidenciasInfo().addTo(map);
	          		data.setStyle(getStyle);
	          		$rootScope.INCIDENCIA_STATE = STATE;
	          		finishedLoading();
	          	});
	          	mapServices.getPoblacionPorDepto($("#sYear").val())
		    		.then(function(data){
		    			STATE.poblacion = data;
		    		}).catch(function(err){
	    				console.log(err);
	    		});
	    		mapServices.getPoblacionPorDistrito($("#sYear").val())
		    		.then(function(data){
		    			STATE.poblacion_distrito = data;
		    		}).catch(function(err){
	    				console.log(err);
	    		});
		    	mapServices.getPoblacionPorBarrioAsu()
		    		.then(function(data){
		    			STATE.poblacion_asu = data;
		    		}).catch(function(err){
	    				console.log(err);
	    		});
	          	
			} else {
				mapCtrl.setSelectedEstado(STATE.clasificacion);
				departamentos = mapCtrl.getDepartamentos();
				departamentos.then(function(data){
					STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature: onEachFeature}).addTo(map);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
					$("#sMonth").val(STATE.semana);
					$("#sYear").val(STATE.anio);
					return init(STATE.semana, STATE.anio, STATE.geoJsonLayer);
				}).then(function(data){
	          		data.setStyle(getStyle);
	          		if(STATE.inZoom){
	          			var distritos;
	          			if(STATE.dptoDrillDown == 'ASUNCION'){
					    	var distritos = mapServices.getBarrios(STATE.dptoDrillDown);
					    } else {
					    	var distritos = mapServices.getDistritos(STATE.dptoDrillDown);
					    }
	          			distritos.then(function(data){
	          				STATE.drillDownLayer = L.geoJson(data, {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
	          				STATE.drillDownLayer.addTo(map);
	          				map.fitBounds(STATE.drillDownLayer.getBounds());
							STATE.backButton = new backClass();
					        map.addControl(STATE.backButton);
					        mapCtrl.addControl(STATE.backButton);
							mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
	          				}).catch(function(err){
			    				console.log(err);
			    		});
	          			finishedLoading();
					}
					STATE.info.addTo(map);
					STATE.incidenciaLegend.addTo(map);
					mapCtrl.addControl(STATE.info);
					mapCtrl.addControl(STATE.incidenciaLegend);
					mapCtrl.setActualLayer(STATE.geoJsonLayer);
	          	});
			}


        	$("#sMonth").on({
				slide: function(){
					reloadSem($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});
			
			var backClass =  L.Control.extend({
		        options: {
		            position: 'topright'
		        },

		        onAdd: function (map) {
		            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
		            L.DomEvent
		                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
		                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
		            .addListener(controlDiv, 'click', drillUp);

		            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-interior btn btn-sm btn-primary', controlDiv);
		            controlUI.title = 'Volver a incidencias por dptos';
		            return controlDiv;
		        }
		    });
		    

			function init(sem, ano, layer){
	  			var defered = $q.defer();
	      		var promise = defered.promise;
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			return mapServices.getIncidenciaDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosIncidencia =  data;
	    				return mapServices.getIncidenciaDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					console.log("incidencias de distritos- ");
	  					STATE.distritosIncidencia = data;
	  					return mapServices.getIncidenciaAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					console.log("incidencias de asuncion- ");
	  					STATE.asuncionIncidencia = data;
	  					reloadSem(STATE.semana);
	  					defered.resolve(layer);
	  					return promise;
	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	    		
	  		}

	  		function reloadAnio(sem, ano){
	  			STATE.semana = sem;
	  			STATE.anio = ano;
	  			mapServices.getIncidenciaDepartamentos(STATE.anio)
	  				.then(function(data){
	    				STATE.departamentosIncidencia =  data;
	    				return mapServices.getIncidenciaDistritos(STATE.anio);
	    			})
	  				.then(function(data){
	  					console.log("incidencias de distritos- ");
	  					STATE.distritosIncidencia = data;
	  					return mapServices.getIncidenciaAsuncion(STATE.anio);
	  				})
	  				.then(function(data){
	  					console.log("incidencias de asuncion- ");
	  					STATE.asuncionIncidencia = data;
	  					reloadSem(STATE.semana);

	  				})
	    			.catch(function(err){
	    				console.log(err);
	    			});
	    		mapServices.getPoblacionPorDepto(STATE.anio)
		    		.then(function(data){
		    			STATE.poblacion = data;
		    		}).catch(function(err){
	    				console.log(err);
	    			});
	    		mapServices.getPoblacionPorDistrito(STATE.anio)
		    		.then(function(data){
		    			STATE.poblacion_distrito = data;
		    		}).catch(function(err){
	    				console.log(err);
	    			});
	  		}

	  		

			function reloadSem(semana){
				STATE.semana = semana;
			    var rasu;
			    var rcen;
			    var mapSem = new Object();
			    var mapSemDis = new Object();

			    //Cargar el json de los incidencias por semana
			    //incidencias de todas las semanas de los departamentos
			    var incidencia = STATE.departamentosIncidencia;
			    //incidencias de todas las semana de los distritos
			    var incidenciaDis = STATE.distritosIncidencia;
			    //incidencias de todas las semanas de los barrios de asuncion
			    var incidenciaAsu = STATE.asuncionIncidencia;
			   	console.log("reloadSem - - -");

			    for(var i=0; i<incidencia.length; i++){
			        var obj = incidencia[i];
			        if(obj["semana"]== semana ){
			            mapSem[obj["departamento"]]= obj;
			            if(obj["departamento"]=="CENTRAL"){
			                rcen = obj;
			            }
			            if(obj["departamento"]=="ASUNCION"){
			                rasu = obj;
			            }
			        }
			    }
			    //console.log(rasu);

			    //incidencia de los departamentos de la semana seleccionada
			    STATE.incidenciaSemanaDep = mapSem;
			   //console.log(mapSem);
			    for(var i=0; i<incidenciaDis.length; i++){
			        var obj = incidenciaDis[i];
			        if(obj["semana"]== semana ){
			            mapSemDis[obj["distrito"]]= obj;
			        }
			    }
			    //Incluir en el mapSemDis los barrios de Asuncion
			    
			    for(var i=0; i<incidenciaAsu.length; i++){
			        var obj = incidenciaAsu[i];
			        if(obj["semana"] == semana){
			            mapSemDis[obj["barrio"]] = obj;
			        }

			    }
			    //console.log(mapSemDis);
			   	//incidencias de los distritos de la semana seleccionada
			    STATE.incidenciaSemanaDis = mapSemDis;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    if(STATE.inZoom){
			    	STATE.drillDownLayer.setStyle(getStyleDrillDown);
			    }
			}

	  		function getIncidenciasLegend(){
	  			STATE.incidenciaLegend = L.control({
			        position: 'bottomright'
			    });
			    STATE.incidenciaLegend.onAdd = function(map) {
			    	this._div = L.DomUtil.create('div', 'info legend');
			        this.update();
			        return this._div;
			    };
			    STATE.incidenciaLegend.update = function(){
			    	
			    	if(STATE.inZoom){
			    		this._div.innerHTML = '<span>Tasa de incidencia</span><br/><small>cada 10.000 hab.</small>'
			    			+'<br><i style="background:' + getColor(30) + '"></i> ' + '>= 30'
				        	+'<br><i style="background:' + getColor(21) + '"></i> ' + '20 - 30' // >200
				        	+'<br><i style="background:' + getColor(11) + '"></i> ' + '10 - 20' // >100
				        	+'<br><i style="background:' + getColor(9)  + '"></i> ' + '<10' // <100
				        	+'<br><i style="background:' + getColor(0) + '"></i> ' + '0'; // == 0
			    	} else {
			    		this._div.innerHTML = '<span>Tasa de incidencia</span><br/><small>cada 100.000 hab.</small>'
			    			+'<br><i style="background:' + getColor(300) + '"></i> ' + '>= 300'
				        	+'<br><i style="background:' + getColor(210) + '"></i> ' + '200 - 300' // >200
				        	+'<br><i style="background:' + getColor(110) + '"></i> ' + '100 - 200' // >100
				        	+'<br><i style="background:' + getColor(10) + '"></i> ' + '<100' // <100
				        	+'<br><i style="background:' + getColor(0) + '"></i> ' + '0'; // == 0
			    	}
			    	
			    };
			    mapCtrl.addControl(STATE.incidenciaLegend);
			    return STATE.incidenciaLegend;
	  		}

	  		function getIncidenciasInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info info-layer');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {
			    	
			        if(props){
			            var dep = props.dpto_desc;
			            var mapSem = STATE.incidenciaSemanaDep;
			            var nroNS = '0';
			            var pob = '-';
			            var casos = '0';
			            var clas = clasificaciones[STATE.clasificacion];
		            	
		                pob = formattNumber(STATE.poblacion[parseInt(props.dpto)].total);
			            
			            try{
			                nroNS = mapSem[dep][STATE.clasificacion].toString().replace(/\./g, ',');
			                casos = mapSem[dep][clas.casos];
			            }catch(e){
			            	
			            }
			          this._div.innerHTML =  '<b>Mapa de Incidencia Dengue</b><br/>'
			          					+ '<b>Año:</b> '+ STATE.anio 
			          					+ ' <b>Semana:</b> ' + STATE.semana 
			          					+ '<br/><b>Dpto:</b> ' + dep 
			          					+ '<br/><b>Incidencia:</b> ' + nroNS + '<small>/100.000 hab.</small><br/>'
			          					+ '<b>Población:</b> ' + pob + '<br/>'
			          					+ '<b>Casos:</b> ' + casos 
			          					+ '<small> (' + clas.legend + ')</small><br/>';
			        }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){
			            var dep = props.dpto_desc, depAsu = props.dpto_desc, dis = props.dist_desc;
			            var mapSem = STATE.incidenciaSemanaDis, nroNS = '0', pob = '-', casos = '0', info;
			            var clas = clasificaciones[STATE.clasificacion];
			            if(depAsu == 'ASUNCION'){
			            	dis = props.dist_desc;
			                var key = depAsu+'-'+props.barlo_desc;
			                var keyP = parseInt(props.bar_loc).toString();
			                pob = formattNumber(STATE.poblacion_asu[keyP.toString()].total);
			                try{
			                    nroNS = mapSem[key][STATE.clasificacion].toString().replace(/\./g, ',');
			                    casos = mapSem[key][clas.casos];
			                
			                }catch(e){

			                }
			                info =  '<b>Mapa de Incidencia Dengue</b><br/>'
			                	+ '<b>Año: </b>' + STATE.anio 
			                	+ ' - <b>Semana: </b>' + STATE.semana 
			                	+ '<br/><b>Dpto: </b>' + depAsu 
			                	+ '<br/><b>Distrito: </b>' + dis 
			                	+ '<br/><b>Barrio:</b> ' + props.barlo_desc 
			                	+ '<br/><b>Incidencia:</b> '+ nroNS +'<small>/10.000 hab.</small>'
			                	+ '<br/><b>Población:</b> ' + pob + '<br/>'
			          			+ ' <b>Casos:</b> ' + casos 
			          			+ '<small> (' + clas.legend + ')</small><br/>';
			            } else {
			                var key = dep+'-'+dis;
			                var keyP = parseInt(props.dpto).toString() + parseInt(props.distrito).toString();
			                pob = formattNumber(STATE.poblacion_distrito[keyP.toString()].total);
			                try{
			                    nroNS = mapSem[key][STATE.clasificacion].toString().replace('\.', ',');
			                    casos = mapSem[key][clas.casos];
			                    
			                }catch(e){

			                }
			                info = '<b>Mapa de Incidencia Dengue</b><br/>'
			                	+ '<b>Año:</b> ' + STATE.anio 
			                	+' - <b>Semana:</b> '+ STATE.semana 
			                	+ '<br/><b>Dpto:</b> ' + dep 
			                	+ '<br/><b>Distrito:</b> ' + dis 
			                	+ '<br/><b>Incidencia:</b> ' + nroNS + '<small>/10.000 hab.</small>'
			                	+ '<br><b>Población:</b> ' + pob + '<br/>'
			          			+ ' <b>Casos:</b> ' + casos
			          			+ '<small> (' + clas.legend + ')</small><br/>';
			            }
			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }

			function onEachFeature(feature, layer) {
			    layer.on({
			        mouseover: mouseover,
			        mouseout: mouseout,
			        click: zoomToFeature
			    });

			}

			function mouseover(e) {
				var layer = e.target;
				if(!STATE.inZoom || STATE.dptoDrillDown!=layer.feature.properties.dpto_desc){
				     layer.setStyle({
				        weight: 5,
				        color: '#666',
				        dashArray: ''
				    });

				    if (!L.Browser.ie && !L.Browser.opera) {
				        layer.bringToFront();
				    }
				    STATE.info.update(layer.feature.properties);
			    }
			}

			/*Evento al salir el puntero de un departamento*/
			function mouseout(e) {
			    //geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();

			}

			/*Zoom al hacer click en un departamento*/
			function zoomToFeature(e) {
				
			    var target = e.target;
			    startLoading();
			    if(!STATE.inZoom){
				   	STATE.backButton = new backClass();
			        map.addControl(STATE.backButton);
			        mapCtrl.addControl(STATE.backButton);
			    }
			    if(target.feature.properties.dpto_desc == 'ASUNCION'){
			    	 mapServices.getBarrios(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					   	STATE.incidenciaLegend.update();
					   	finishedLoading();
	    			});
			    } else {
			    	console.log('zoom:',target.feature.properties.dpto);
			    	mapServices.getDistritos(target.feature.properties.dpto)
			    	.then(function(data){
			    		console.log('data:', data);
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					    STATE.incidenciaLegend.update();
					    finishedLoading();
	    			});
			    }
			    STATE.dptoDrillDown = target.feature.properties.dpto_desc;
			    

			}

	    	function getColor(d) {
	    		if(STATE.inZoom){
	    			return d >= 30 ? '#FE0516' :
			        d >= 20 ? '#FF6905' :
			        d >= 10 ? '#FFB905' :
			        d == 0 ? '#FEFEFF':
			        '#FFF96D';
	    		}

			    return d >= 300 ? '#FE0516' :
			        d >= 200 ? '#FF6905' :
			        d >= 100 ? '#FFB905' :
			        d == 0 ? '#FEFEFF':
			        '#FFF96D';
			}

			/*Estilo de la capa de departamento de acuerdo a los valores de incidencia*/
			function getStyle(feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.incidenciaSemanaDep;
			    var color = 0;
			    try{
			        color = mapSem[n][STATE.clasificacion];
			    }catch(e){
			    }
			    if(STATE.inZoom){
			    	return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0,
			            fillColor: getColor(color)
			        };
			    }
			  	else{
			  		return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			           	fillOpacity: 0.5,
			            fillColor: getColor(color)
			        };
			  	}

			}

			/*Estilo de la capa de distritos de acuedo a los valores de incidencia*/
			function getStyleDrillDown(feature) {
			    var prop = feature.properties;
			    var n = prop.dpto_desc+'-'+prop.dist_desc;
			    var mapSem = STATE.incidenciaSemanaDis;
			    var color = 0;
			    if(prop.dpto_desc == 'ASUNCION'){
			        n = prop.dpto_desc+'-'+prop.barlo_desc;
			    }
			   try{
			        color = mapSem[n][STATE.clasificacion];
			       
			    }catch(e){
			    }
			   
			    return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0.5, 
			            fillColor: getColor(color) 
			        };
			}
			/*Eventos para cada distrito*/
			function onEachFeatureDrillDown(feature, layer) {
			    layer.on({
			        mouseover: mouseoverDrillDown,
			        mouseout: mouseoutDrillDown,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverDrillDown(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''
			        
			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.updateDrillDown(layer.feature.properties);
			}
			/*Evento al salir el puntero de un distrito*/
			function mouseoutDrillDown(e) {
			    STATE.drillDownLayer.resetStyle(e.target);
			    STATE.info.updateDrillDown();
			   
			}
			function drillUp(){
			    //map.fitBounds(SMV.STATE.geoJsonLayer.getBounds());
			    map.removeLayer(STATE.drillDownLayer);
			    STATE.drillDownLayer = undefined;
			   	STATE.inZoom = false;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    STATE.backButton.removeFrom(map);
			    mapCtrl.removeControl(STATE.backButton);
			    map.setView([-23.388, -60.189], 6);
			    STATE.incidenciaLegend.update();
			}

			function formattNumber(x) {
			    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
			}

			$rootScope.$on('estadoChange', function(data, valor) {
				console.log(valor);
				STATE.clasificacion = valor;
    			STATE.geoJsonLayer.setStyle(getStyle);
			    if(STATE.inZoom){
			    	STATE.drillDownLayer.setStyle(getStyleDrillDown);
			    }
    		});
	    }
	};
}]);
'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('map', ["$rootScope", "$http", "mapServices", "$q", "usSpinnerService", function ($rootScope,  $http, mapServices, $q, usSpinnerService) {
  	 return {
		restrict: 'E',
		replace: false,
		scope: {
			selectedEstado:'='
		},
        templateUrl: 'views/templates/map.html',
        controller: ["$scope", function($scope) {

        	console.log("------ Entro al controller de la directiva map");

        	$scope.tab = "";
        	$scope.activeTab = "";
        	$scope.estados = ["incidencia", "incidencia_sospechosos", "incidencia_confirmados"];
        	$scope.selectedEstado = $scope.estados[0];
        	$scope.estadosLabel = new Object();
        	$scope.estadosLabel["incidencia"] = "confirmados y sospechosos";
        	$scope.estadosLabel["incidencia_sospechosos"] = "sospechosos";
        	$scope.estadosLabel["incidencia_confirmados"] = "confirmados";

		    $scope.isTab = function(tab) {
		    	//console.log("-------- isTab");
		        return tab === $scope.tab;
		    };

		    $scope.setTab = function(tab){
		    	//console.log("-------- setTab");
		    	//$scope.setActiveTab();
		    	if (tab === $scope.tab) {
		    		$scope.tab = "";
		    	} else {
		    		$scope.tab = tab;
		    	}
		    	$scope.activeTab = tab;
		    };
		    
		    this.setSelectedEstado = function(estado){
		    	$scope.selectedEstado = estado;
		    };

		    this.imprimir = function(text){
		    	console.log("METODO LLAMADO DESDE LA DIRECTIVA HIJA");
		    	console.log(text);
		    };

		    this.getMap = function() {
		    	return $scope.MECONF.map;
		    };

		    this.setMap = function(mapa) {
		    	$scope.MECONF.map = mapa;
		    };

		    this.getDepartamentos = function() {
		    	return $scope.MECONF.departamentos;
		    };

		    this.addControl = function(control) {
		    	$scope.controllers.push(control);
		    };

		    this.removeControl = function(control){
		    	$scope.controllers.pop(control);
		    }

		    this.getActualLayer = function() {
		    	return $scope.MECONF.actualLayer;
		    };

		    this.setActualLayer = function(layer) {
		    	$scope.MECONF.actualLayer = layer;
		    };

		    this.saveRiesgoState = function(state) {
		    	$scope.MECONF.riesgoState = state;
		    };

		    this.getRiesgoState = function() {
		    	return $scope.MECONF.riesgoState;
		    };

		   /* this.getActualInfo = function() {
		    	return $scope.MECONF.actualInfo;
		    };

		    this.setActualInfo = function(info) {
		    	$scope.MECONF.actualInfo = info;
		    };

		    this.getActualLegend = function() {
		    	return $scope.MECONF.actualLegend;
		    };

		    this.setActualLegend = function(legend) {
		    	$scope.MECONF.actualLegend = legend;
		    };*/

		    this.getDrillDownLayer = function() {
		    	return $scope.MECONF.drillDownLayer;
		    };

		    this.setDrillDownLayer = function(layer) {
		    	$scope.MECONF.drillDownLayer = layer;
		    };

		    this.cleanMap = function() {
		    	console.log("drillDownLayer");
		    	console.log($scope.MECONF.drillDownLayer);
		    	if($scope.MECONF.drillDownLayer){
		    		$scope.MECONF.map.removeLayer($scope.MECONF.drillDownLayer);
		    		$scope.MECONF.drillDownLayer = undefined;
		    	}
		    	console.log("actualLayer");
		    	console.log($scope.MECONF.actualLayer);

		    	if($scope.MECONF.actualLayer){
		    		$scope.MECONF.map.removeLayer($scope.MECONF.actualLayer);
		    		$scope.MECONF.actualLayer = undefined;
		    	}
		    	var i;
		    	for (i=0;i<$scope.controllers.length;i++) {
		    		var control = $scope.controllers[i];
		    		//console.log(control);
		    		try{
						control.removeFrom($scope.MECONF.map);
		    		} catch(e){
		    			console.log('control ya removido');
		    		}
					
				}
				$scope.controllres = [];
		    	/*if($scope.MECONF.actualLegend){
		    		controllers.push($scope.MECONF.actualLegend);
		    		$scope.MECONF.actualLegend.removeFrom($scope.MECONF.map);
		    		$scope.MECONF.actualLegend = undefined;
		    	}
		    	if($scope.MECONF.actualInfo){
		    		controllers.push($scope.MECONF.actualInfo);
		    		$scope.MECONF.actualInfo.removeFrom($scope.MECONF.map);
		    		$scope.MECONF.actualInfo = undefined;
		    	}*/

		    }


        }],
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {

                    var MECONF = MECONF || {};
			        MECONF.tilesLoaded = false;

			        MECONF.LAYERS = function () {
			            var mapbox = L.tileLayer(
			                    'http://api.tiles.mapbox.com/v4/rparra.jmk7g7ep/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg');
			            var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {minZoom: 3});
			            var gglHybrid = new L.Google('HYBRID');
			            var gglRoadmap = new L.Google('ROADMAP');
			            return {
			                MAPBOX: mapbox,
			                OPEN_STREET_MAPS: osm,
			                GOOGLE_HYBRID: gglHybrid,
			                GOOGLE_ROADMAP: gglRoadmap
			            }
			        };

			        var finishedLoading = function() {
			          if(tilesLoaded){
			            usSpinnerService.stop('spinner-dropdown');
			            MECONF.tilesLoaded = false;
			          }

			        };

			        var startLoading = function() {
			          usSpinnerService.spin('spinner-dropdown');
			        };

			        var setup_gmaps = function() {
			          google.maps.event.addListenerOnce(this._google, 'tilesloaded', tilesLoaded);
			        };

			        var tilesLoaded = function(){
			          MECONF.tilesLoaded = true;
			         // finishedLoading();
			          
			        }

			      	startLoading();

			      	L.mapbox.accessToken = 'pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg';
			      	var layers = MECONF.LAYERS();
			      	var mapbox = layers.MAPBOX.on('load', tilesLoaded);
			      	var osm = layers.OPEN_STREET_MAPS.on('load', tilesLoaded);

			      	var gglHybrid = layers.GOOGLE_HYBRID.on('MapObjectInitialized', setup_gmaps);
			      	var gglRoadmap = layers.GOOGLE_ROADMAP.on('MapObjectInitialized', setup_gmaps);

			      	/* Layer para probar opacidad */
			      	/*var historic_seattle = new L.tileLayer.wms('http://demo.lizardtech.com/lizardtech/iserv/ows', {
			              layers: 'Seattle1890',
			              maxZoom: 18,
			              format: 'image/png',
			              transparent: true
			          });*/

			      	var map = L.map('map', {maxZoom: 18, minZoom: 3, worldCopyJump: true, attributionControl: false, zoomControl: false})
			            .setView([-24, -62.189], 6)
			            //.setView([47.59, -122.30], 12)
			            .on('baselayerchange', startLoading);
			      	new L.Control.Zoom({ position: 'topright' }).addTo(map);

			      	var baseMaps = {
			       		'Calles OpenStreetMap': osm,
			        	'Terreno': mapbox,
			        	'Satélite': gglHybrid,
			        	'Calles Google Maps': gglRoadmap,
			      	};

			      	map.addLayer(osm);

					L.control.layers(baseMaps).addTo(map);
					var departamentos;


			      	MECONF.departamentos = mapServices.getDepartamentos();

		          	map.on('baselayerchange', function (data){
		          		console.log('cambiando mapa base', data.layer);
		          		/*if(!MECONF.actualBaseLayer){
		          			var button = new noneLayer();
				    		scope.noLaButton = map.addControl(button);
		          		}*/
		          		scope.MECONF.actualBaseLayer = data.layer;
		          		
		          	});
		          	
		          	var removeBaseLayer = function(){
        				if(MECONF.actualBaseLayer){
        					map.removeLayer(MECONF.actualBaseLayer);
        					MECONF.actualBaseLayer = undefined;
        					/*console.log('noneLayer',scope.noLaButton);
        					if(scope.noLaButton){
        						scope.noLaButton.removeFrom(map);
        					}*/
        				}
        				
        			}
        			var noneLayer =  L.Control.extend({
				        options: {
				            position: 'topright'
				        },

			        	onAdd: function (map) {
				            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
				            L.DomEvent
				                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
				                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
				            .addListener(controlDiv, 'click', removeBaseLayer);

				            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-base btn btn-sm btn-primary', controlDiv);
				            controlUI.title = 'Sin fondo';
				            return controlDiv;
			        	}
			   		});

			   		var button = new noneLayer();
				    scope.noLaButton = map.addControl(button);

			      	var sidebar = L.control.sidebar('sidebar').addTo(map);

			      	MECONF.map = map;
			      	MECONF.actualBaseLayer = osm;
			      	MECONF.actualLayer = undefined;
			      	MECONF.actualInfo = undefined;
			      	MECONF.actualLegend = undefined;
			      	scope.MECONF = MECONF;
			      	//$rootScope.MAP = MECONF.map;


			      	console.log(MECONF.map);
			      	var mapSlider;
					var selectedYear = 2013;
					var selectedWeek = 1;
					$("#sYear").noUiSlider({
						start: 2013,
						direction: "ltr",
						step: 1,
						range: {
							'min': 2009,
							'max': 2015
						},
						format: wNumb({
							decimals: 0
						})
					});
					$('#sYear').noUiSlider_pips({
						mode: 'steps',
						density: 4
					});

					$("#sMonth").noUiSlider({
						start: 25,
						step: 1,
						direction: "ltr",
						range: {
							'min': 1,
							'max': 53
						},
						format: wNumb({
							decimals: 0
						})
					});
					$('#sMonth').noUiSlider_pips({
						mode: 'count',
						values: 6,
						density: 4
					});


					$("#sYear").Link('lower').to($("#fYear"));
					$("#sMonth").Link('lower').to($("#fMonth"));
					
					scope.tab = "map8";
    				scope.activeTab = "map8";
					
        			scope.controllers = [];
        			
        			
				    scope.change = function(valor){
		        		$rootScope.$broadcast('estadoChange', valor);
		        	};

			  	}
			}
        },
        link: function postLink(scope, element, attrs){
        	console.log("postLink del padre!!")
        	
			
        }

    }
}]);