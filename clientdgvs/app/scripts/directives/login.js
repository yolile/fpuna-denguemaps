'use strict';

angular.module('denguemapsApp')
  .directive("loginForm", [ function () {
      return {
        restrict: 'E',
        scope: {},
        template: '<div class="row"> <div class="col-xs-12"> <form name="loginForm"> <div class="form-group"> <label for="username">Username</label> <input type="text" name="username" ng-model="username" required="required" class="form-control"/> </div> <div class="form-group"> <label for="password">Password</label> <input type="password" name="password" ng-model="password" required="required" class="form-control"/> </div> <button type="submit" ng-disabled="!loginForm.$valid" ng-class="{\'btn-default\':!loginForm.$valid,\'btn-success\':loginForm.$valid}" ng-click="login()" class="btn login">Login</button> <span ng-show="loginError && !loginForm.$valid" class="label label-danger"> <b>Error With Login</b> Please try again. </span></form> </div> </div>',
        replace: 'true',
        controller: ['$scope', '$http', '$window', '$rootScope', '$location', 'AuthenticationService',
          function($scope, $http, $window, $rootScope, $location, AuthenticationService) {

            AuthenticationService.ClearCredentials();

            $scope.login = function () {
              $scope.dataLoading = true;
              AuthenticationService.Login($scope.username, $scope.password, function(response) {
                console.log(response);
                if(response) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/datos');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                    alert("No se ha podido autenticar.");
                }
              });
            };

          }]
      }
    }])