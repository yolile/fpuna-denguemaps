'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('map', function ($rootScope,  $http, mapServices, $q, usSpinnerService) {
  	 return {
		restrict: 'E',
		replace: false,
		scope: {
			selectedEstado:'='
		},
        templateUrl: 'views/templates/map.html',
        controller: function($scope) {

        	console.log("------ Entro al controller de la directiva map");

        	$scope.tab = "";
        	$scope.activeTab = "";
        	$scope.estados = ["incidencia", "incidencia_sospechosos", "incidencia_confirmados"];
        	$scope.selectedEstado = $scope.estados[0];
        	$scope.estadosLabel = new Object();
        	$scope.estadosLabel["incidencia"] = "confirmados y sospechosos";
        	$scope.estadosLabel["incidencia_sospechosos"] = "sospechosos";
        	$scope.estadosLabel["incidencia_confirmados"] = "confirmados";

		    $scope.isTab = function(tab) {
		    	//console.log("-------- isTab");
		        return tab === $scope.tab;
		    };

		    $scope.setTab = function(tab){
		    	//console.log("-------- setTab");
		    	//$scope.setActiveTab();
		    	if (tab === $scope.tab) {
		    		$scope.tab = "";
		    	} else {
		    		$scope.tab = tab;
		    	}
		    	$scope.activeTab = tab;
		    };
		    
		    this.setSelectedEstado = function(estado){
		    	$scope.selectedEstado = estado;
		    };

		    this.imprimir = function(text){
		    	console.log("METODO LLAMADO DESDE LA DIRECTIVA HIJA");
		    	console.log(text);
		    };

		    this.getMap = function() {
		    	return $scope.MECONF.map;
		    };

		    this.setMap = function(mapa) {
		    	$scope.MECONF.map = mapa;
		    };

		    this.getDepartamentos = function() {
		    	return $scope.MECONF.departamentos;
		    };

		    this.addControl = function(control) {
		    	$scope.controllers.push(control);
		    };

		    this.removeControl = function(control){
		    	$scope.controllers.pop(control);
		    }

		    this.getActualLayer = function() {
		    	return $scope.MECONF.actualLayer;
		    };

		    this.setActualLayer = function(layer) {
		    	$scope.MECONF.actualLayer = layer;
		    };

		    this.saveRiesgoState = function(state) {
		    	$scope.MECONF.riesgoState = state;
		    };

		    this.getRiesgoState = function() {
		    	return $scope.MECONF.riesgoState;
		    };

		   /* this.getActualInfo = function() {
		    	return $scope.MECONF.actualInfo;
		    };

		    this.setActualInfo = function(info) {
		    	$scope.MECONF.actualInfo = info;
		    };

		    this.getActualLegend = function() {
		    	return $scope.MECONF.actualLegend;
		    };

		    this.setActualLegend = function(legend) {
		    	$scope.MECONF.actualLegend = legend;
		    };*/

		    this.getDrillDownLayer = function() {
		    	return $scope.MECONF.drillDownLayer;
		    };

		    this.setDrillDownLayer = function(layer) {
		    	$scope.MECONF.drillDownLayer = layer;
		    };

		    this.cleanMap = function() {
		    	console.log("drillDownLayer");
		    	console.log($scope.MECONF.drillDownLayer);
		    	if($scope.MECONF.drillDownLayer){
		    		$scope.MECONF.map.removeLayer($scope.MECONF.drillDownLayer);
		    		$scope.MECONF.drillDownLayer = undefined;
		    	}
		    	console.log("actualLayer");
		    	console.log($scope.MECONF.actualLayer);

		    	if($scope.MECONF.actualLayer){
		    		$scope.MECONF.map.removeLayer($scope.MECONF.actualLayer);
		    		$scope.MECONF.actualLayer = undefined;
		    	}
		    	var i;
		    	for (i=0;i<$scope.controllers.length;i++) {
		    		var control = $scope.controllers[i];
		    		//console.log(control);
		    		try{
						control.removeFrom($scope.MECONF.map);
		    		} catch(e){
		    			console.log('control ya removido');
		    		}
					
				}
				$scope.controllres = [];
		    	/*if($scope.MECONF.actualLegend){
		    		controllers.push($scope.MECONF.actualLegend);
		    		$scope.MECONF.actualLegend.removeFrom($scope.MECONF.map);
		    		$scope.MECONF.actualLegend = undefined;
		    	}
		    	if($scope.MECONF.actualInfo){
		    		controllers.push($scope.MECONF.actualInfo);
		    		$scope.MECONF.actualInfo.removeFrom($scope.MECONF.map);
		    		$scope.MECONF.actualInfo = undefined;
		    	}*/

		    }


        },
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {

                    var MECONF = MECONF || {};
			        MECONF.tilesLoaded = false;

			        MECONF.LAYERS = function () {
			            var mapbox = L.tileLayer(
			                    'http://api.tiles.mapbox.com/v4/rparra.jmk7g7ep/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg');
			            var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {minZoom: 3});
			            var gglHybrid = new L.Google('HYBRID');
			            var gglRoadmap = new L.Google('ROADMAP');
			            return {
			                MAPBOX: mapbox,
			                OPEN_STREET_MAPS: osm,
			                GOOGLE_HYBRID: gglHybrid,
			                GOOGLE_ROADMAP: gglRoadmap
			            }
			        };

			        var finishedLoading = function() {
			          if(tilesLoaded){
			            usSpinnerService.stop('spinner-dropdown');
			            MECONF.tilesLoaded = false;
			          }

			        };

			        var startLoading = function() {
			          usSpinnerService.spin('spinner-dropdown');
			        };

			        var setup_gmaps = function() {
			          google.maps.event.addListenerOnce(this._google, 'tilesloaded', tilesLoaded);
			        };

			        var tilesLoaded = function(){
			          MECONF.tilesLoaded = true;
			         // finishedLoading();
			          
			        }

			      	startLoading();

			      	L.mapbox.accessToken = 'pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg';
			      	var layers = MECONF.LAYERS();
			      	var mapbox = layers.MAPBOX.on('load', tilesLoaded);
			      	var osm = layers.OPEN_STREET_MAPS.on('load', tilesLoaded);

			      	var gglHybrid = layers.GOOGLE_HYBRID.on('MapObjectInitialized', setup_gmaps);
			      	var gglRoadmap = layers.GOOGLE_ROADMAP.on('MapObjectInitialized', setup_gmaps);

			      	/* Layer para probar opacidad */
			      	/*var historic_seattle = new L.tileLayer.wms('http://demo.lizardtech.com/lizardtech/iserv/ows', {
			              layers: 'Seattle1890',
			              maxZoom: 18,
			              format: 'image/png',
			              transparent: true
			          });*/

			      	var map = L.map('map', {maxZoom: 18, minZoom: 3, worldCopyJump: true, attributionControl: false, zoomControl: false})
			            .setView([-24, -62.189], 6)
			            //.setView([47.59, -122.30], 12)
			            .on('baselayerchange', startLoading);
			      	new L.Control.Zoom({ position: 'topright' }).addTo(map);

			      	var baseMaps = {
			       		'Calles OpenStreetMap': osm,
			        	'Terreno': mapbox,
			        	'Satélite': gglHybrid,
			        	'Calles Google Maps': gglRoadmap,
			      	};

			      	map.addLayer(osm);

					L.control.layers(baseMaps).addTo(map);
					var departamentos;


			      	MECONF.departamentos = mapServices.getDepartamentos();

		          	map.on('baselayerchange', function (data){
		          		console.log('cambiando mapa base', data.layer);
		          		/*if(!MECONF.actualBaseLayer){
		          			var button = new noneLayer();
				    		scope.noLaButton = map.addControl(button);
		          		}*/
		          		scope.MECONF.actualBaseLayer = data.layer;
		          		
		          	});
		          	
		          	var removeBaseLayer = function(){
        				if(MECONF.actualBaseLayer){
        					map.removeLayer(MECONF.actualBaseLayer);
        					MECONF.actualBaseLayer = undefined;
        					/*console.log('noneLayer',scope.noLaButton);
        					if(scope.noLaButton){
        						scope.noLaButton.removeFrom(map);
        					}*/
        				}
        				
        			}
        			var noneLayer =  L.Control.extend({
				        options: {
				            position: 'topright'
				        },

			        	onAdd: function (map) {
				            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
				            L.DomEvent
				                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
				                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
				            .addListener(controlDiv, 'click', removeBaseLayer);

				            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-base btn btn-sm btn-primary', controlDiv);
				            controlUI.title = 'Sin fondo';
				            return controlDiv;
			        	}
			   		});

			   		var button = new noneLayer();
				    scope.noLaButton = map.addControl(button);

			      	var sidebar = L.control.sidebar('sidebar').addTo(map);

			      	MECONF.map = map;
			      	MECONF.actualBaseLayer = osm;
			      	MECONF.actualLayer = undefined;
			      	MECONF.actualInfo = undefined;
			      	MECONF.actualLegend = undefined;
			      	scope.MECONF = MECONF;
			      	//$rootScope.MAP = MECONF.map;


			      	console.log(MECONF.map);
			      	var mapSlider;
					var selectedYear = 2013;
					var selectedWeek = 1;
					$("#sYear").noUiSlider({
						start: 2013,
						direction: "ltr",
						step: 1,
						range: {
							'min': 2009,
							'max': 2015
						},
						format: wNumb({
							decimals: 0
						})
					});
					$('#sYear').noUiSlider_pips({
						mode: 'steps',
						density: 4
					});

					$("#sMonth").noUiSlider({
						start: 25,
						step: 1,
						direction: "ltr",
						range: {
							'min': 1,
							'max': 53
						},
						format: wNumb({
							decimals: 0
						})
					});
					$('#sMonth').noUiSlider_pips({
						mode: 'count',
						values: 6,
						density: 4
					});


					$("#sYear").Link('lower').to($("#fYear"));
					$("#sMonth").Link('lower').to($("#fMonth"));
					
					scope.tab = "map8";
    				scope.activeTab = "map8";
					
        			scope.controllers = [];
        			
        			
				    scope.change = function(valor){
		        		$rootScope.$broadcast('estadoChange', valor);
		        	};

			  	}
			}
        },
        link: function postLink(scope, element, attrs){
        	console.log("postLink del padre!!")
        	
			
        }

    }
});