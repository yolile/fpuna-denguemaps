'use strict';

/**
 * @ngdoc overview
 * @name denguemapsApp
 * @description
 * # denguemapsApp
 *
 * Main module of the application.
 */

angular
  .module('denguemapsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'datatables',
    'datatables.bootstrap',
    'ui.bootstrap',
    'angularSpinner',
    'flow',
    'toastr'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/listado', {
        templateUrl: 'views/listado.html',
        controller: 'ListadoCtrl'
      })
      .when('/diccionario', {
        templateUrl: 'views/diccionario.html',
        controller: 'DiccionarioCtrl'
      })
      /*.when('/acercade', {
        templateUrl: 'views/acercade.html',
        controller: 'AcercaDeCtrl'
      })
      .when('/datos', {
        templateUrl: 'views/datos.html',
        controller: 'DatosCtrl',
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'AuthCtrl'
      })*/
      .otherwise({
        redirectTo: '/'
      });
  })
 .run(function ($rootScope, $location, $cookieStore, $http) {
    /* Control de paginas privadas */
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }

    $rootScope.$on('$routeChangeStart', function (ev, next, curr) {
     /* if (next.$$route.originalPath == '/datos') {
        if ($rootScope.globals.currentUser) { $location.path('/datos');}
        else $location.path('/login');
      }*/

    })
  });
 //window.basePath = "http://www.cds.com.py";
 window.basePath = "http://127.0.0.1:8084";
